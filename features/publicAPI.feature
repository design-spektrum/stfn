Feature: Public API

   This describes a "public" API.

   Scenario: Access to API
      When Server want access to API
      Then If server is on allowed list he can access it if not then he should get HTTP 403 with error message
      And It should add server to Waiting for Acceptance list
   
   Scenario: /translate/:lang/:phrase
      Given Phrase to translate
      And Server Name
      And Language
      When Server access /translate API
      Then If phrase exist respond with a phrase, If not exist then create one with default setting and create notifications for admins and respond with phrase as null