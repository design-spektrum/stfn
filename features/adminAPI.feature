Feature: Admin API
  Scenario: /login/authgen
    Given User Name and Password Hash
    When Server requests new Auth Token
    Then Respond with Authentication Token if User Name and Password is correct for any user
  Scenario: /login/
    Given User Name and Password OR Authentication Token 
    When Server requests login
    Then If login was successfull respond with success message and create session Else Throw error