<?php
/**
 * Model Aware Trait
 * @author Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2019 Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2019 PAiP Web
 */
namespace HelperTraits;
/**
 * Model Aware Trait
 * This trait defines function for getting model.
 */
trait ModelAwareTrait
{
    /**
     * Get Model
     * @param string $model Name of Model
     */
    public function model($model)
    {
        // Require Model File
        require_once APPROOT.DIRECTORY_SEPARATOR.'models'.DIRECTORY_SEPARATOR.$model.'.php';
        // Instansiate model
        return new $model();
    }
}