<?php
/**
 * View Aware Trait
 * @author Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2019 Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2019 PAiP Web
 */
namespace HelperTraits;
/**
 * View Aware Trait
 * This trait defines function for getting view.
 */
trait ViewAwareTrait
{
    /**
     * Get View
     * @param string $view Name of View
     * @param array $data Data for View
     */
    public function view($view, $data=[])
    {
        $filename = APPROOT.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.$view.'.php';
        // Check for the view file
        if(file_exists($filename))
        {
            require_once $filename;
        }
        else
        {
            die("View [$view] Doesn't exist!");
        }
    }
}