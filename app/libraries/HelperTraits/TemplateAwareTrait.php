<?php
/**
 * Template Aware Trait
 * @author Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2019 Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2019 PAiP Web
 */
namespace HelperTraits;
/**
 * Template Aware Trait
 * This trait defines function for using template strings.
 */
trait TemplateAwareTrait
{
    /**
     * Converts template string into string
     * @param string $template Template of String like /test/{21}/{test}
     * @param array $param Array of Params [$parameter_from_string => $value_for_parameter]
     * @return string String
     */
    function templateToString($template, $params=array())
    {
        $result = $template;
        foreach($params as $param => $value)
        {
            $result = str_replace("{".$param."}", $value, $result);
        }
        return $result;
    }
}