<?php
/**
 * Authentication Trait
 * @author Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2019 Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2019 PAiP Web
 */
namespace Auth;
/**
 * Auth Trait
 * Authorization trait
 * 
 * This trait is for use in controllers.
 * Requires \HelperTraits\ModelAwareTrait
 */
trait AuthenticationTrait
{
    protected function authenticateByPassword($login, $password)
    {
      $m = $this->model("AuthModel");
      return $m->checkPass($login, $password);
    }
    protected function authenticateByToken($token)
    {
      $m = $this->model("AuthModel");
      return $m->jwt_validate($token);
    }
    protected function makeToken($login)
    {
      $m = $this->model("AuthModel");
      $token = $m->jwt_create($login);
      return $token;
    }
    public function _login($user, $pass)
    {
      if($this->authenticateByPassword($user, $pass))
      {
        $_SESSION["stfn_loggedin"] = true;
        return true;
      }
      else
      {
        $_SESSION["stfn_logerror"] = true;
        return false;
      }
    }
    public function _tokenCreate($username)
    {
      return $this->makeToken($username);
    }
    public function _authlogin($token)
    {
      if(empty($token))
      {
        return null;
      }
      if($this->authenticateByToken($token))
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    public function _logout()
    {
      session_destroy();
      session_unset();
      header("Location: index");
    }
}
