<?php
/**
 * Auth Trait
 * @author Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2019 Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2019 PAiP Web
 */
namespace Auth;
use \HelperTraits\ModelAwareTrait;
/**
 * Auth Trait
 * Authorization trait
 * 
 * This trait is for use in controllers.
 */
trait AuthTrait
{
    use ModelAwareTrait;
    public function isAuthenticated($userGroup=\Auth\Acl::ACL__UG__GUEST)
    {
        // return isset($_SESSION["stfn_loggedin"]) ? $_SESSION["stfn_loggedin"] : false;
        if(isset($_SESSION["stfn_loggedin"]))
        {
            return $_SESSION["stfn_loggedin"];
        }
        $authHeader = \PHP::getHeader("Authorization");
        if($authHeader !== null)
        {
            $authToken = str_replace("Bearer ", "", $authHeader);
            $m = $this->model("AuthModel");
            return $m->jwt_validate($authToken);
        }
        return false;
    }
    public function isAllowed()
    {
        $m = $this->model("ProtectedAuthModel");
        return $m->isAuthed();
    }
    public function checkAllow()
    {
        $this->model("ProtectedAuthModel");
        $auth = $this->isAllowed();
        if($auth === \ProtectedAuthModel::AUTH_DISALLOW)
        {
            $this->respondJSON(
                [
                    "error" => "Your secret name is not on allowed list!",
                    "status" => false,
                ],
                [
                    "status" => $this->_getHTTPStatus(403)
                ]
            );
            exit;
        }
        if($auth === \ProtectedAuthModel::AUTH_FAILED)
        {
            $this->respondJSON(
                [
                    "error" => "Secret name hasn't been specified!",
                    "status" => false,
                ],
                [
                    "status" => $this->_getHTTPStatus(403)
                ]
            );
            exit;
        }
    }
}