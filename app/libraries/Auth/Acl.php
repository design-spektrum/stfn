<?php
/**
 * ACL Class
 * @author Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2019 Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2019 PAiP Web
 */
namespace Auth;
/**
 * ACL Class
 */
class Acl
{
    const ACL__UG__GUEST = 0x0000;
    const ACL__UG__API = 0x1000;
    const ACL__UG__ADMIN = 0xF001;
    const ACL__UG__TRANSLATOR = 0xF002;
    const ACL__UG__ROOT = 0xFFFF;
}