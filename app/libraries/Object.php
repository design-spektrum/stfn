<?php
/**
 * Object Class
 * @author Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2019 Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2019 PAiP Web
 */

/**
 * Object Class
 */
class Object
{
    /**
     * Object Class Contructor
     * @param array $obj Data to make into object
     */
    public function __construct(array $obj)
    {
        foreach($obj as $k => $v)
        {
            $this->$k = $v;
        }
    }
}