<?php
/**
 * API Controller Class
 * This loads the models and views
 * @author Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2018 Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2018 PAiP Web
 */

/**
 * API Controller Class
 */
class APIController
{
    /**
     * HTTP Response with JSON in data
     * @param mixed $data Data to Respond
     * @param array $http HTTP Headers
     */
    public function respondJSON($data, $http=array())
    {
        return APIClass::JSONResponse($data, $http);
    }
    /**
     * HTTP Response
     * @param mixed $data Data to Respond
     * @param array $http HTTP Headers
     */
    public function respond($data, $http=array())
    {
        return APIClass::Response($http, $data);
    }
    /**
     * get HTTP Request Object Array
     * @return array HTTP Request Data
     */
    public function getRequest()
    {
        return APIClass::Request();
    }
    /**
     * Loads Model
     * @param string $model Model which you want to load
     */
    public function model($model)
    {
        // Require Model File
        require_once '../app/models/'.$model.'.php';
        // Instansiate model
        return new $model();
    }
    /**
     * Get HTTP Status Array
     * @param int $code Code to Process
     * @return array HTTP Status Code Array
     */
    public function _getHTTPStatus($code)
    {
        return APIClass::getHTTPStatus($code);
    }
}