<?php
/**
 * NotificationAPI Controller
 * This controller provides routes for main origin.
 * @author Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2019 Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2019 PAiP Web
 * @todo Wersja 1.0 Aplikacji STFN zostanie tego pozbawiona z powodu czasu
 */

use Minishlink\WebPush\WebPush;
use Minishlink\WebPush\Subscription;
/**
 * NotificationAPI Controller
 * This controller provides routes for main origin.
 */
class NotificationAPI extends APIController
{
    /**
     * NotificationAPI constructor.
     */
    public function __construct()
    {
    }
    public function subscribe()
    {
        $subscription = json_decode(file_get_contents('php://input'), true);
        if (!isset($subscription['endpoint'])) {
            echo 'Error: not a subscription';
            return;
        }
        $method = $_SERVER['REQUEST_METHOD'];
        switch ($method) {
            case 'POST':
                // create a new subscription entry in your database (endpoint is unique)
                break;
            case 'PUT':
                // update the key and token of subscription corresponding to the endpoint
                break;
            case 'DELETE':
                // delete the subscription corresponding to the endpoint
                break;
            default:
                echo "Error: method not handled";
                return;
        }
        // here I'll get the subscription endpoint in the POST parameters
        // but in reality, you'll get this information in your database
        // because you already stored it (cf. push_subscription.php)
        $subscription = Subscription::create(json_decode(file_get_contents('php://input'), true));
        $auth = array(
            'VAPID' => array(
                'subject' => 'mailto: patrykadamczyk@patrykadamczyk.net',
                'publicKey' => file_get_contents(APPROOT.'/data/keys/public.txt'), // don't forget that your public key also lives in app.js
                'privateKey' => file_get_contents(APPROOT.'/data/keys/private.txt'), // in the real world, this would be in a secret file
            ),
        );
        $webPush = new WebPush($auth);
        $res = $webPush->sendNotification(
            $subscription,
            json_encode(array(
                "title" => "Test Notification",
                "content" => "Hello"
            ))
        );
        // handle eventual errors here, and remove the subscription from your server if it is expired
        foreach ($webPush->flush() as $report) {
            $endpoint = $report->getRequest()->getUri()->__toString();
            if ($report->isSuccess()) {
                echo "[v] Message sent successfully for subscription {$endpoint}.";
            } else {
                echo "[x] Message failed to sent for subscription {$endpoint}: {$report->getReason()}";
            }
        }
        $this->respondJSON([],["status" => $this->_getHTTPStatus(201)]);
    }
    /**
     * @todo
     */
    public function push_subscription()
    {

        $subscription = json_decode(file_get_contents('php://input'), true);

        if (!isset($subscription['endpoint'])) {
            echo 'Error: not a subscription';
            return;
        }
        
        $method = $_SERVER['REQUEST_METHOD'];
        
        switch ($method) {
            case 'POST':
                // create a new subscription entry in your database (endpoint is unique)
                break;
            case 'PUT':
                // update the key and token of subscription corresponding to the endpoint
                break;
            case 'DELETE':
                // delete the subscription corresponding to the endpoint
                break;
            default:
                echo "Error: method not handled";
                return;
        }        
        // $this->send_push_notification();
    }
    /**
     * @todo
     */
    public function send_push_notification()
    {
        // here I'll get the subscription endpoint in the POST parameters
        // but in reality, you'll get this information in your database
        // because you already stored it (cf. push_subscription.php)
        $subscription = Subscription::create(json_decode(file_get_contents('php://input'), true));

        $auth = array(
            'VAPID' => array(
                'subject' => 'https://github.com/Minishlink/web-push-php-example/',
                'publicKey' => file_get_contents(APPROOT.'/data/keys/public.txt'), // don't forget that your public key also lives in app.js
                'privateKey' => file_get_contents(APPROOT.'/data/keys/private.txt'), // in the real world, this would be in a secret file
            ),
        );

        $webPush = new WebPush($auth);

        $res = $webPush->sendNotification(
            $subscription,
            "Hello!"
        );

        // handle eventual errors here, and remove the subscription from your server if it is expired
        foreach ($webPush->flush() as $report) {
            $endpoint = $report->getRequest()->getUri()->__toString();

            if ($report->isSuccess()) {
                echo "[v] Message sent successfully for subscription {$endpoint}.";
            } else {
                echo "[x] Message failed to sent for subscription {$endpoint}: {$report->getReason()}";
            }
        }

    }
}