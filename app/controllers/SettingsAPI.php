<?php
/**
 * SettingsAPI Controller
 * @author Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2019 Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2019 PAiP Web
 */
use \Auth\AuthTrait;
/**
 * SettingsAPI Controller
 */
class SettingsAPI extends APIController
{
    use AuthTrait;
    /**
     * SettingsAPI constructor.
     */
    public function __construct()
    {
    }

    /**
     * Main Route
     * /SettingsAPI/getsettings
     * This route provides you Main Page of whole application.
     */
    public function getsettings()
    {
        $this->checkAllow();
        $data = [
            "settings" => [
                "DEVELOPMENT" => DEVELOPMENT,
                "PROJECT_MODE" => PROJECT_MODE,
                "VERBOSE" => VERBOSE,
                "DB_HOST" => DB_HOST,
                "DB_NAME" => DB_NAME,
                "APPROOT" => APPROOT,
                "URLROOT" => URLROOT,
                "PROJECT_VERSION" => PROJECT_VERSION,
                "PROJECT_FULL_VERSION" => PROJECT_FULL_VERSION,
                "PROJECT_API_VERSION" => PROJECT_API_VERSION
            ],
            "status" => true
        ];
        $this->respondJSON($data);
    }
}