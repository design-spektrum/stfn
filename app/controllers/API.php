<?php
/**
 * API Controller
 * @author Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2019 Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2019 PAiP Web
 */

/**
 * API Controller
 * Authorization is needed
 */
class API extends APIController
{
    // Use AuthTrait
    use \Auth\AuthTrait;
    /**
     * API Controller constructor.
     */
    public function __construct()
    {
    }
    /**
     * Index Route
     * /api/
     * This route gives you all translations saved in a Model.
     */
    public function index()
    {
        // $model = $this->model("TranslationModel");
        // $_SESSION["test3"] = true;
        // $data = [
        //     "session" => $_SESSION,
        //     "server" => $_SERVER
        // ];
        // $this->respondJSON($data);
    }
    /**
     * Get all translations
     * /api/translations/
     * This route gives you all translations saved in Database.
     * 
     * ATTENTION: This route requires authentication.
     */
    public function translations()
    {
        // Authorization
        $this->isAuthenticated();
        // Translation Model
        $model = $this->model("TranslationModel");
        // Return all results from translations
        $this->respondJSON(
            [
                "result" => $model->all(),
                "status" => true,
                "__API_VERSION__" => PROJECT_API_VERSION
            ],
            [
                "status" => $this->_getHTTPStatus(200)
            ]
        );
    }
    /**
     * Get all notifications
     * /api/notifications/
     * This route gives you all notifications saved in Database.
     * 
     * ATTENTION: This route requires authentication.
     */
    public function notifications()
    {
        // Authorization
        $this->isAuthenticated();
        // Notifications Model
        $model = $this->model("NotificationsModel");
        // Return all results from Notifications
        $this->respondJSON(
            [
                "result" => $model->all(),
                "status" => true,
                "__API_VERSION__" => PROJECT_API_VERSION
            ],
            [
                "status" => $this->_getHTTPStatus(200)
            ]
        );
    }
    /**
     * Translation RESTful API
     * /api/translation/
     * This route gives you CRUD REST API.
     * 
     * ATTENTION: This route requires authentication.
     */
    public function translation()
    {
        // Authorization
        $this->isAuthenticated();
        // Request Method
        $method = $_SERVER["REQUEST_METHOD"];
        // Translation Model
        $m = $this->model("TranslationModel");
        // Switch for CRUD REST API
        switch($method)
        {
            case "GET":
                // READ API from CRUD
                //TID
                if(isset($_GET["tid"]))
                {
                    $result = $m->getByTID($_GET["tid"]);
                    return $this->respondJSON(
                        [
                            "status" => $result["status"],
                            "result" => $result
                        ],
                        [
                            "status" => $this->_getHTTPStatus(200)
                        ]
                    );
                }
                // Check
                if(!isset($_GET["lang"]))
                {
                    return $this->respondJSON(
                        [
                            "status" => false,
                            "error" => "Language not specified"
                        ],
                        [
                            "status" => $this->_getHTTPStatus(400)
                        ]
                    );
                }
                if(!isset($_GET["phrase"]))
                {
                    return $this->respondJSON(
                        [
                            "status" => false,
                            "error" => "Phrase not specified"
                        ],
                        [
                            "status" => $this->_getHTTPStatus(400)
                        ]
                    );
                }
                // Read One Translation
                $result = $m->get($_GET["lang"], $_GET["phrase"]);
                return $this->respondJSON(
                    [
                        "status" => $result["status"],
                        "result" => $result
                    ],
                    [
                        "status" => $this->_getHTTPStatus(200)
                    ]
                );
            
            case "POST":
                // CREATE API from CRUD
                $data = json_decode(file_get_contents("php://input"), true);
                $data = array_merge($data, $_REQUEST);
                // Check
                if(!isset($data["lang"]))
                {
                    return $this->respondJSON(
                        [
                            "status" => false,
                            "error" => "Language not specified"
                        ],
                        [
                            "status" => $this->_getHTTPStatus(400)
                        ]
                    );
                }
                if(!isset($data["phrase"]))
                {
                    return $this->respondJSON(
                        [
                            "status" => false,
                            "error" => "Phrase not specified"
                        ],
                        [
                            "status" => $this->_getHTTPStatus(400)
                        ]
                    );
                }
                // Model
                $status = $m->add([
                    "lang" => $data["lang"],
                    "translation" => isset($data["translation"]) ? $data["translation"] : NULL,
                    "phrase" => $data["phrase"]
                ]);
                return $this->respondJSON(
                    [
                        "status" => $status
                    ],
                    [
                        "status" => $status ? $this->_getHTTPStatus(201) : $this->_getHTTPStatus(500)
                    ]
                );
            
            case "PUT":
                // UPDATE API from CRUD
                $reqData = json_decode(file_get_contents("php://input"), true);
                $reqData = array_merge($reqData, $_REQUEST);
                // Check
                if(!isset($reqData["tid"]))
                {
                    return $this->respondJSON(
                        [
                            "status" => false,
                            "error" => "TID not specified"
                        ],
                        [
                            "status" => $this->_getHTTPStatus(400)
                        ]
                    );
                }
                /*
                if(!isset($reqData["lang"]))
                {
                    return $this->respondJSON(
                        [
                            "status" => false,
                            "error" => "Language not specified"
                        ],
                        [
                            "status" => $this->_getHTTPStatus(400)
                        ]
                    );
                }
                if(!isset($reqData["phrase"]))
                {
                    return $this->respondJSON(
                        [
                            "status" => false,
                            "error" => "Phrase not specified"
                        ],
                        [
                            "status" => $this->_getHTTPStatus(400)
                        ]
                    );
                }
                if(!isset($reqData["translation"]))
                {
                    return $this->respondJSON(
                        [
                            "status" => false,
                            "error" => "Translation not specified"
                        ],
                        [
                            "status" => $this->_getHTTPStatus(400)
                        ]
                    );
                }
                */
                $data = [
                    "TID" => $reqData["tid"]
                ];
                if(isset($reqData["lang"]))
                {
                    $data["T_Language"] = $reqData["lang"];
                }
                if(isset($reqData["phrase"]))
                {
                    $data["T_Phrase"] = $reqData["phrase"];
                }
                if(isset($reqData["translation"]))
                {
                    $data["Translation"] = $reqData["translation"];
                }
                $result = $m->update($data);
                return $this->respondJSON(
                    [
                        "status" => $result
                    ],
                    [
                        "status" => $result ? $this->_getHTTPStatus(200) : $this->_getHTTPStatus(500)
                    ]
                );

            case "DELETE":
                // DELETE API from CRUD
                if(isset($_GET["tid"]))
                {
                    $m->delete($_GET["tid"]);
                    return $this->respondJSON(
                        [
                            "status" => true,
                        ],
                        [
                            "status" => $this->_getHTTPStatus(200)
                        ]
                    );
                }
                else
                {
                    return $this->respondJSON(
                        [
                            "status" => false,
                            "error" => "TID not specified"
                        ],
                        [
                            "status" => $this->_getHTTPStatus(400)
                        ]
                    );
                }

            case "OPTIONS":
                return $this->respondJSON(
                    [
                        "status" => true,
                    ],
                    [
                        "status" => $this->_getHTTPStatus(200)
                    ]
                );
            
            default:
                return $this->respondJSON(
                    [
                        "error" => "Method not allowed",
                        "method" => $method,
                        "status" => false
                    ],
                    [
                        "status" => $this->_getHTTPStatus(405)
                    ]
                );
        }
    }
}