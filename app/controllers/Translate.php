<?php
/**
 * Translation Controller
 * This controller provides routes for main origin.
 * @author Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2019 Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2019 PAiP Web
 */
use \Auth\AuthTrait;
/**
 * Translate Controller
 * This controller provides routes for Public Translation API.
 * No Autorization is needed to get anything from that controller.
 */
class Translate extends APIController
{
    use AuthTrait;
    /**
     * Translate Controller constructor.
     */
    public function __construct()
    {
    }
    /**
     * Get Translation Route
     * /translate/{lang}/{phrase}
     * This route gives you translation for specified phrase.
     * @param string $lang Language Code [Route Param]
     * @param string $phrase Phrase to Translate [Route Param | REQUEST DATA]
     */
    public function index($lang=null, $phrase=null)
    {
        $this->checkAllow();
        $notifications = $this->model("Notification");
        if(empty($lang))
        {
            $notifications->notify(
                $notifications::TYPE_SERVER_ERRORCALL,
                array(
                    "server" => $_SERVER["HTTP_HOST"]
                ),
                "Język nie został podany!"
            );
            $this->respondJSON(
                [
                    "error" => "Language hasn't been specified!",
                    "status" => false,
                    "result" => null
                ],
                [
                    "status" => $this->_getHTTPStatus(400)
                ]
            );
            exit;
        }
        if(empty($phrase))
        {
            $phrase = isset($_REQUEST["phrase"]) ? $_REQUEST["phrase"] : null;
        }
        if(empty($phrase))
        {
            $notifications->notify(
                $notifications::TYPE_SERVER_ERRORCALL,
                array(
                    "server" => $_SERVER["HTTP_HOST"]
                ),
                "Fraza nie została podana!"
            );
            $this->respondJSON(
                [
                    "error" => "Phrase not specified!",
                    "status" => false,
                    "result" => null
                ],
                [
                    "status" => $this->_getHTTPStatus(400)
                ]
            );
            exit;
        }
        $model = $this->model("TranslationModel");
        $result = $model->get($lang, $phrase);
        if($result["status"] === true)
        {
            $res = $result;
            unset($res["tid"]);
            $notifications->notify(
                $notifications::TYPE_SERVER_REQUEST,
                array(
                    "server" => $_SERVER["HTTP_HOST"],
                    "phrase" => $phrase,
                    "lang" => $lang
                ),
                "Fraza znaleziona!"
            );
            if($result["phrase"] === $result["translation"])
            {
                $notifications->notify(
                    $notifications::TYPE_SERVER_PROBABLY_UNTRANSLATED,
                    array(
                        "server" => $_SERVER["HTTP_HOST"],
                        "phrase" => $phrase,
                        "lang" => $lang
                    ),
                    "Fraza możliwe że nie została poprawnie przetłumaczona!"
                );
            }
            $this->respondJSON(
                [
                    "result" => $res,
                    "status" => true,
                    "__API_VERSION__" => PROJECT_API_VERSION
                ],
                [
                    "status" => $this->_getHTTPStatus(200)
                ]
            );
        }
        else
        {
            $model->add([
                "lang" => $lang,
                "translation" => NULL,
                "phrase" => $phrase
            ]);
            $notifications->notify(
                $notifications::TYPE_SERVER_UNTRANSLATED,
                array(
                    "server" => $_SERVER["HTTP_HOST"],
                    "phrase" => $phrase,
                    "lang" => $lang
                ),
                "Fraza nie znaleziona lub nie przetłumaczona!"
            );
            $this->respondJSON(
                [
                    "status" => false,
                    "result" => null
                ],
                [
                    "status" => $this->_getHTTPStatus(404)
                ]
            );
        }
    }
}