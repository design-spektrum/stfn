<?php
/**
 * @author Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2018 Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2018 PAiP Web
 */
class RA9_PW_Tools extends APIController
{
    use \Auth\AuthenticationTrait;
    use \Auth\AuthTrait;
    // private function isAuthenticated()
    // {
    //     return false;
    // }
    public function install()
    {
        if($this->isAuthenticated())
        {
            // Here Will be instalation Process
        }
        else
        {
            $this->respond(
                "You are not allowed to go here!",
                array(
                    "status" => $this->_getHTTPStatus(403)
                )
            );
        }
    }
    /**
     * Password Route
     * /RA9_PW_Tools/password/:pass
     * It generating hash for provided password.
     * This route respond with JSON.
     */
    public function password($pass)
    {
        header('Content-Type: application/json');
        $passh = password_hash($pass, PASSWORD_DEFAULT);
        print(json_encode([
            $pass => $passh
        ]));
    }
    /**
     * Version Route
     * /RA9_PW_Tools/generate_version/:ver
     * It generating two lines for config with specified version in format used in PWF.
     * This route respond with lines which can be copypaste directly to /app/config/config.php .
     */
    public function generate_version($version)
    {
        echo("<pre>");
        $fullversion = $version."+".date("YmdHis");
        print("Wersja: \n\n\n");
        print("define(\"PROJECT_VERSION\", \"$version\");");
        print("\n");
        print("define(\"PROJECT_FULL_VERSION\", \"$fullversion\");");
        echo("</pre>");
    }
}