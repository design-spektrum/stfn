<?php
/**
 * Administration Panel Controller
 * This controller provides routes for main origin.
 * @author Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2019 Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2019 PAiP Web
 */

/**
 * Admin Controller
 * This controller provides routes for Public Translation API.
 * No Autorization is needed to get anything from that controller.
 */
class Admin extends APIController
{
    // Use AuthTrait
    use \Auth\AuthTrait;
    // Use AuthenticationTrait
    use \Auth\AuthenticationTrait;
    // Use ViewAwareTrait by using API Controller instead of Controller
    use \HelperTraits\ViewAwareTrait;
    //
    public function index()
    {
        if($this->isAuthenticated())
        {
            $this->view("admin/react/index");
        }
        else
        {
            // $this->view("admin/zaloguj");
            $this->login();
        }
    }
    public function login()
    {
        $data = [
            "body-class" => "text-center",
            "title" => "Panel Administracyjny Projektu STFN",
            "css-stylesheets-head" => [
                "https://fonts.googleapis.com/icon?family=Material+Icons",
                "https://fonts.googleapis.com/css?family=Roboto+Condensed|Ubuntu+Condensed",
                "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css",
                "https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.4/css/tether.min.css",
                "https://use.fontawesome.com/releases/v5.0.13/css/all.css"
            ],
            "author" => "Patryk Adamczyk",
            "copyright" => "Patryk Adamczyk © 2019",
            "contact" => "developer@paipweb.com",
            "html-id" => "login-panel"
        ];
        $this->view("admin/login", $data);
    }
    public function loginform()
    {
        $username = isset($_REQUEST["username"]) ? $_REQUEST["username"] : null;
        $password = isset($_REQUEST["password"]) ? $_REQUEST["password"] : null;
        if($username === null || $password === null)
        {
            $this->respond(
                "No username or password specified",
                [
                    "status" => $this->_getHTTPStatus(400)
                ]
            );
        }
        $res = $this->_login($username, $password);
        if($res)
        {
            header("Location: index");
        }
        else
        {
            $this->respond(
                "Wrong Password",
                [
                    "status" => $this->_getHTTPStatus(403)
                ]
            );
        }
    }
    public function logout()
    {
        $this->_logout();
    }
    public function getJWT()
    {
        $username = "React";
        $this->respond(
            "Bearer ".$this->_tokenCreate($username)
        );
        // return $this->_tokenCreate($username);
    }
}