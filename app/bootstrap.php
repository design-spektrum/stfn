<?php
/**
 * Bootstraping Script
 * @author Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2018 Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2018 PAiP Web
 */
// Load Config
require_once "config/config.php";

// Autoload Core Libraries
spl_autoload_register(function($className){
    // Linux has problems
    $className = str_replace("\\", DIRECTORY_SEPARATOR, $className);
    $filename = __DIR__."/libraries/".$className.".php";
    // var_dump($className, $filename);
    if(file_exists($filename))
    {
        require_once $filename;
    }
    else
    {
        return false;
    }
});