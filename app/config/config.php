<?php
// Require PWF Information Class
/**
 * Development Variable
 */
define("DEVELOPMENT", true);
/**
 * Project State
 * 
 * 2 - Dev
 * 1 - Staging
 * 0 - Production
 */
define("PROJECT_MODE", 2);
/**
 * Verbose Level Variable
 * 
 * 0 - Normal
 * 1 - Normal (in PWF meaning you can add your own in views or anything else)
 * 2 - Verbose
 * 3 - More Verbose
 * 255 - Verbose Debug Mode
 */
define("VERBOSE", 3);
/*************** Database ***************/
if(DEVELOPMENT) # Test & Dev
{
    // // Database HostName
    // define('DB_HOST', 'localhost');
    // // Database User
    // define('DB_USER', 'root');
    // // Database Password
    // define('DB_PASS', 'admin');
    // // Database Name
    // define('DB_NAME', 'stfn');
    // Database HostName
    define('DB_HOST', 'localhost');
    // Database User
    define('DB_USER', '18805385_stfn');
    // Database Password
    define('DB_PASS', 'hVa5nJ1iD24HDuzgqgSbtQKz');
    // Database Name
    define('DB_NAME', '18805385_stfn');
}
else
{
    // Database HostName
    define('DB_HOST', 'localhost');
    // Database Name
    define('DB_NAME', '');
    // Database User
    define('DB_USER', '');
    // Database Password
    define('DB_PASS', '');
}
/*************** PATH ***************/
// Application Root
define("APPROOT", dirname(dirname(__FILE__)));
if(DEVELOPMENT) # Test & Dev
{
    // URL Root
    // define("URLROOT", 'http://192.168.1.199:90/stfn');
    define("URLROOT", 'https://serwer1523088.home.pl/stfn');
}
else
{
    // URL Root
    define("URLROOT", '');
}
/************** Project Information *************/
// Project Version
// define("PROJECT_VERSION", "0.0dev1");
// define("PROJECT_FULL_VERSION", "0.0dev1+20190212094409");
/**
 * Project Version
 */
define("PROJECT_VERSION", "1.0beta1");
/**
 * Project Full Version
 */
define("PROJECT_FULL_VERSION", "1.0beta1+20190226104928");
/**
 * API Version
 * 
 * ATTENTION:
 * * Don't mess with that version until you understand what SemVer is *
 * 
 * SemVer 2.0.0 is used here
 * https://semver.org/spec/v2.0.0.html
 * 
 * dev is special version which is used for development.
 */
define("PROJECT_API_VERSION", "1.0");
/******************** S3CR3T5 *******************/
define("SECRET_UUID", "0617b15d-b130-4713-8b35-b64b7b1a935c");