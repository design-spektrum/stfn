<?php
/**
 * Protected Auth Model Class
 * @author Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2019 Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2019 PAiP Web
 */

/**
 * Protected Auth Model Class
 */
class ProtectedAuthModel
{
    const ACCESS_DISALLOWED = 0xFF;
    const ACCESS_ALLOWED = 0xFE;
    const ACCESS_DEFAULT_ALLOWED = 0xFC;
    const ACCESS_WAITING = 0x01;
    const ACCESS_NULL = 0x00;
    const AUTH_ALLOW = true;
    const AUTH_DISALLOW = false;
    const AUTH_FAILED = null;
    /**
     * ProtectedAuthModel Constructor
     */
    public function __construct()
    {
    }
    public function isAuthed()
    {
      $secretName = null;
      if(isset($_SERVER["HTTP_S3CR3TN4M3"]))
      {
        $secretName = $_SERVER["HTTP_S3CR3TN4M3"];
      }
      elseif(isset($_REQUEST["S3CR3TN4M3"]))
      {
        $secretName = $_REQUEST["S3CR3TN4M3"];
      }
      else
      {
        return self::AUTH_FAILED;
      }
      $res = $this->get($secretName);
      $_ = $res["acceptance"] == self::ACCESS_ALLOWED || $res["acceptance"] == self::ACCESS_DEFAULT_ALLOWED;
      if($_)
      {
        return self::AUTH_ALLOW;
      }
      else
      {
        return self::AUTH_DISALLOW;
      }
    }
    public function get($secretName)
    {
      $ret = null;
      //Database Check
      //Default Check
      $defaults = $this->defaultAllowed();
      if($ret === null && in_array($secretName, $defaults))
      {
        $ret = [
          "name" => $secretName,
          "acceptance" => self::ACCESS_DEFAULT_ALLOWED
        ];
      }
      return $ret;
    }
    public function add($secretName)
    {
    }
    public function delete($secretName, $id=NULL)
    {
    }
    public function all()
    {
    }
    public function defaultAllowed()
    {
      $allowed = [
        "dev.local",
        SECRET_UUID
      ];
      return $allowed;
    }
}