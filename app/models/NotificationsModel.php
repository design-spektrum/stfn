<?php
/**
 * Notifications Model Class
 * @author Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2019 Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2019 PAiP Web
 */

/**
 * Notifications Model Class
 */
class NotificationsModel
{
    /**
     * NotificationsModel Constructor
     */
    public function __construct()
    {
    }
    /**
     * Recursive Database Result Object To Result Converter
     * @param array $dro Database Result Object
     * @param string $type Type of Return
     * @return object|array Result
     */
    public function r_DRO2R(array $_dro, $type="array")
    {
        $res = [];
        foreach($_dro as $k => $dro)
        {
            $res[$k] = $this->DRO2R($dro, $type);
        }
        return $res;
    }
    /**
     * Database Result Object To Result Converter
     * @param array $dro Database Result Object
     * @param string $type Type of Return
     * @return object|array Result
     */
    public function DRO2R(array $dro, $type="array")
    {
        $result = [];
        $result['status'] = !empty($dro);
        $result['nid'] = isset($dro["NID"]) ? $dro["NID"] : null;
        $result['context'] = isset($dro["N_Context"]) ? $dro["N_Context"] : null;
        $result['title'] = isset($dro["N_Title"]) ? $dro["N_Title"] : null;
        $result['content'] = isset($dro["N_Content"]) ? $dro["N_Content"] : null;
        $result['date'] = isset($dro["N_Date"]) ? $dro["N_Date"] : null;
        $result["state"] = isset($dro["N_STATE"]) ? $dro["N_STATE"] : null;
        if($type==="array")
        {
            return $result;
        }
        elseif($type==="object")
        {
            return new Object($result);
        }
        else
        {
            return $result;
        }
    }
    /**
     * Empty Database Result Object To Result Converter
     * @param string $type Type of Return
     * @return object|array Result
     */
    public function e_DRO2R($type="array")
    {
        $result = [
            'status' => false
        ];
        if($type==="array")
        {
            return $result;
        }
        elseif($type==="object")
        {
            return new Object($result);
        }
        else
        {
            return $result;
        }
    }
    /**
     * Get Notification
     * @param string $nid
     * @return mixed Database Result Object
     */
    public function get($nid)
    {
        $dbm = new Database(PDO::FETCH_ASSOC);
        if($lang === NULL)
        {
            $lang = "%";
        }
        if($phrase === NULL)
        {
            return $this->all();
        }
        $dbm->query("SELECT * FROM stfn_notifications WHERE NID=:nid");
        $dbm->bind("nid", $nid);
        $res = $dbm->single();
        // return $this->DRO2R($res);
        return $res ? $this->DRO2R($res) : $this->e_DRO2R();
    }
    /**
     * Adding Notification
     * @param string $title Title of Notification
     * @param string $content Content of Notification
     * @param string $context Context of Notification
     * @return bool Is it processed
     */
    public function add($title, $content, $context=null)
    {
        $dbm = new Database(PDO::FETCH_ASSOC);
        $sql = "INSERT INTO stfn_notifications(N_Title,N_Content";
        if($context !== null)
        {
            $sql .= ", N_Context";
        }
        $sql .= ") VALUES (:title, :content";
        if($context !== null)
        {
            $sql .= ", :context";
        }
        $sql .= ")";
        $dbm->query($sql);
        $dbm->bind("title", $title);
        $dbm->bind("content", $content);
        if($context !== null)
        {
          $dbm->bind("context", $context);
        }
        $dbm->execute();
        return true;
    }
    /**
     * Deleting Notification
     * @param int|string $id Notification Identifier
     * @return mixed Database Result Object
     */
    public function delete($nid)
    {
        $dbm = new Database(PDO::FETCH_ASSOC);
        $dbm->query("DELETE FROM stfn_notifications WHERE NID=:nid");
        $dbm->bind($nid);
        return $dbm->execute();
    }
    /**
     * Getting All Results
     * @return mixed Database Result Object
     */
    public function all()
    {
        $dbm = new Database(PDO::FETCH_ASSOC);
        $dbm->query("SELECT * FROM stfn_notifications");
        $res = $dbm->resultSet();
        return $res ? $this->r_DRO2R($res) : $this->e_DRO2R();
    }
}