<?php
/**
 * Notification Class
 * @author Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2019 Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2019 PAiP Web
 */

/**
 * Notification Class
 */
class Notification
{
    // Need for Template Strings
    use \HelperTraits\TemplateAwareTrait;
    // Need for Model Method
    use \HelperTraits\ModelAwareTrait;
    // Configuration Constants
    const CONFIG_DONT_NOTIFY = [
        self::TYPE_SERVER_REQUEST,
        self::TYPE_SERVER_PROBABLY_UNTRANSLATED
    ];
    /**
     * Notification Constructor
     */
    public function __construct()
    {
    }
    // Types
    /**
     * Type of Notification when Server receives Unauthenticated Request.
     */
    const TYPE_SERVER_UNAUTHENTICATED = "TYPE_SERVER_UNAUTHENTICATED";
    /**
     * Type of Notification when Server receives New Phrase.
     */
    const TYPE_SERVER_UNTRANSLATED = "TYPE_SERVER_UNTRANSLATED";
    /**
     * Type of Notification when Server receives Request for probably untranslated phrase.
     */
    const TYPE_SERVER_PROBABLY_UNTRANSLATED = "TYPE_SERVER_PROBABLY_UNTRANSLATED";
    /**
     * Type of Notification when Server receives Request which is broken in data.
     */
    const TYPE_SERVER_ERRORCALL = "TYPE_SERVER_ERRORCALL";
    /**
     * Type of Notification when Server receives Normal Request to API.
     */
    const TYPE_SERVER_REQUEST = "TYPE_SERVER_REQUEST";

    /**
     * Make notification
     * @param string $type Type of Notification
     * @param array $params Parameters for Template
     * @param string $context Context of Call (optional)
     * @return bool Success
     */
    public function notify($type, $params, $context=null)
    {
        if(!in_array($type, self::CONFIG_DONT_NOTIFY))
        {
            $dbm = $this->model("NotificationsModel");
            list($title, $content) = $this->template($type, $params);
            return $dbm->add($title, $content, $context);
        }
        return false;
    }
    /**
     * Make custom notification
     * @param string $type Type of Notification (Title)
     * @param string $content Content of Notification
     * @param string $context Context of Call (optional)
     * @return bool Success
     */
    public function cnotify($type, $content, $context=null)
    {
        $dbm = $this->model("NotificationsModel");
        return $dbm->add($title, $content, $context);
    }
    /**
     * Template
     * @param string $type Type of Notification Template
     * @param array $params Parameters for Template
     * @return array (TITLE, CONTENT)
     */
    public function template($type, $params)
    {
        $title = $this->templateToString($this->getTemplateTitle($type), $params);
        $content = $this->templateToString($this->getTemplate($type), $params);
        return array($title, $content);
    }
    /**
     * Content Template
     * @param string $type Type of Notification Template
     * @param array $params Parameters of Template
     * @return string Content
     */
    public function ctemplate($type, $params)
    {
        $content = $this->templateToString($this->getTemplate($type), $params);
        return $content;
    }
    /**
     * Get Template String
     * @param string $type Type of Template
     * @return string Template String
     */
    public function getTemplate($type)
    {
        $template = "{content}";
        switch($type)
        {
            case self::TYPE_SERVER_UNAUTHENTICATED:
                $template = "Serwer {server} próbował się połączyć do API.";
                break;
            case self::TYPE_SERVER_UNTRANSLATED:
                $template = "Serwer {server} prosił o przetłumaczenie frazy {phrase}, której nie było w systemie.";
                break;
            case self::TYPE_SERVER_PROBABLY_UNTRANSLATED:
                $template = "Serwer {server} prosił o przetłumaczenie frazy {phrase}, która prawdopodobnie nie została przetłumaczona poprawnie.";
                break;
            case self::TYPE_SERVER_ERRORCALL:
                $template = "Serwer {server} utworzył nie prawidłowe zapytanie do API. Sprawdź czy obsługa dostępu do api została zrobiona poprawnie lub czy nie zmieniałeś systemu API.";
                break;
            case self::TYPE_SERVER_REQUEST:
                $template = "Serwer {server} utworzył prawidłowe zapytanie do API.";
                break;
            default:
                break;
        }
        return $template;
    }
    /**
     * Get Template Title String
     * @param string $type Type of Template
     * @return string Template String
     */
    public function getTemplateTitle($type)
    {
        $template = "{content}";
        switch($type)
        {
            case self::TYPE_SERVER_UNAUTHENTICATED:
                $template = "Błąd Autentykacji!";
                break;
            case self::TYPE_SERVER_UNTRANSLATED:
                $template = "Fraza nie została przetłumaczona!";
                break;
            case self::TYPE_SERVER_PROBABLY_UNTRANSLATED:
                $template = "Fraza prawdopodbnie została źle przetłumaczona.";
                break;
            case self::TYPE_SERVER_ERRORCALL:
                $template = "Błąd zapytania do API!";
                break;
            case self::TYPE_SERVER_REQUEST:
                $template = "Prawidłowe Zapytanie o Tłumaczenie";
                break;
            default:
                break;
        }
        return $template;
    }
}