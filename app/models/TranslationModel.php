<?php
/**
 * Translation Model Class
 * @author Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2019 Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2019 PAiP Web
 */

/**
 * Translation Model Class
 */
class TranslationModel
{
    /**
     * TranslationModel Constructor
     */
    public function __construct()
    {
    }
    /**
     * Recursive Database Result Object To Result Converter
     * @param array $dro Database Result Object
     * @param string $type Type of Return
     * @return object|array Result
     */
    public function r_DRO2R(array $_dro, $type="array")
    {
        $res = [];
        foreach($_dro as $k => $dro)
        {
            $res[$k] = $this->DRO2R($dro, $type);
        }
        return $res;
    }
    /**
     * Database Result Object To Result Converter
     * @param array $dro Database Result Object
     * @param string $type Type of Return
     * @return object|array Result
     */
    public function DRO2R(array $dro, $type="array")
    {
        $result = [];
        $result['status'] = !empty($dro) && isset($dro["Translation"]) ? true : false;
        $result['tid'] = isset($dro["TID"]) ? $dro["TID"] : null;
        $result['phrase'] = isset($dro["T_Phrase"]) ? $dro["T_Phrase"] : null;
        $result['lang'] = isset($dro["T_Language"]) ? $dro["T_Language"] : null;
        $result['translation'] = isset($dro["Translation"]) ? $dro["Translation"] : null;
        if($type==="array")
        {
            return $result;
        }
        elseif($type==="object")
        {
            return new Object($result);
        }
        else
        {
            return $result;
        }
    }
    /**
     * Empty Database Result Object To Result Converter
     * @param string $type Type of Return
     * @return object|array Result
     */
    public function e_DRO2R($type="array")
    {
        $result = [
            'status' => false
        ];
        if($type==="array")
        {
            return $result;
        }
        elseif($type==="object")
        {
            return new Object($result);
        }
        else
        {
            return $result;
        }
    }
    /**
     * Get Translation
     * @param string $phrase Phrase you want to get Translation for
     * @param string $lang Language you want to get Translation to
     * @param string $project Project from which you want to get translation from
     * @return mixed Database Result Object
     */
    public function get($lang, $phrase)
    {
        $dbm = new Database(PDO::FETCH_ASSOC);
        if($lang === NULL)
        {
            $lang = "%";
        }
        if($phrase === NULL)
        {
            return $this->all();
        }
        $dbm->query("SELECT * FROM stfn_translations WHERE T_Phrase=:phrase AND T_Language=:lang");
        $dbm->bind("phrase", $phrase);
        $dbm->bind("lang", $lang);
        $res = $dbm->single();
        // return $this->DRO2R($res);
        return $res ? $this->DRO2R($res) : $this->e_DRO2R();
    }
    /**
     * Get Translation
     * @param string $tid TID
     * @return mixed Database Result Object
     */
    public function getByTID($tid)
    {
        $dbm = new Database(PDO::FETCH_ASSOC);
        $dbm->query("SELECT * FROM stfn_translations WHERE TID=:tid");
        $dbm->bind("tid", $tid);
        $res = $dbm->single();
        // return $this->DRO2R($res);
        return $res ? $this->DRO2R($res) : $this->e_DRO2R();
    }
    /**
     * Adding Translation
     * 
     * Attention:
     * ! translation in $data is required
     * @param array $data Data to Add to database
     * @return bool Is it processed
     */
    public function add($data)
    {
        if(!isset($data["phrase"]) && !is_array($data))
        {
            die("TranslationModel Error: Phrase not specified!");
            exit();
            return false;
        }
        $dbm = new Database(PDO::FETCH_ASSOC);
        $sql = "INSERT INTO stfn_translations(T_Phrase";
        if(isset($data["lang"]))
        {
            $sql .= ", T_Language";
        }
        $sql .= ", Translation) VALUES (:phrase";
        if(isset($data["lang"]))
        {
            $sql .= ", :lang";
        }
        if($data["translation"] === null)
        {
            $sql .= ", NULL";
        }
        else
        {
            $sql .= ", :translation";
        }
        $sql .= ")";
        $dbm->query($sql);
        foreach($data as $k => $v)
        {
            if($v !== null)
            {
                $dbm->bind($k, $v);
            }
        }
        $dbm->execute();
        return true;
    }
    /**
     * Adding Translation
     * 
     * Attention:
     * ! tid is required
     * @param array $data Data to Add to database
     * @return bool Is it processed
     */
    public function update($data)
    {
        if(!isset($data["tid"]) && !is_array($data))
        {
            die("TranslationModel Error: TID not specified!");
            exit();
            return false;
        }
        $dbm = new Database(PDO::FETCH_ASSOC);
        $sql = "UPDATE stfn_translations SET ";
        foreach($data as $k => $v)
        {
            if($v === null)
            {
                $sql .= $k."=NULL,";
            }
            if($k !== "TID")
            {
                $sql .= $k."=:".$k.",";
            }
        }
        $sql = substr($sql, 0, -1);
        $sql .= " WHERE TID=:tid";
        $dbm->query($sql);
        foreach($data as $k => $v)
        {
            if($k === "TID")
            {
                $k = strtolower($k);
                $v = (int) $v;
            }
            if($v !== null)
            {
                $dbm->bind($k, $v);
            }
        }
        $dbm->execute();
        return true;
    }
    /**
     * Deleting Translation
     * @param int|string $id Translation Identifier
     * @return mixed Database Result Object
     */
    public function delete($id)
    {
        $dbm = new Database(PDO::FETCH_ASSOC);
        $dbm->query("DELETE FROM stfn_translations WHERE TID=:tid");
        $dbm->bind("tid", $id);
        return $dbm->execute();
    }
    /**
     * Getting All Results
     * @return mixed Database Result Object
     */
    public function all()
    {
        $dbm = new Database(PDO::FETCH_ASSOC);
        $dbm->query("SELECT * FROM stfn_translations");
        $res = $dbm->resultSet();
        return $res ? $this->r_DRO2R($res) : $this->e_DRO2R();
    }
}