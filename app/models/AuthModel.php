<?php
/**
 * Auth Model Class
 * @author Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2019 Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2019 PAiP Web
 */

/**
 * Auth Model Class
 */
class AuthModel
{
    /**
     * AuthModel Constructor
     */
    public function __construct()
    {
    }
    /**
     * Checking Password
     * @param string $username User Name for Login
     * @param string $password User Password for Login
     * @return bool Is password correct and user exist
     */
    public function checkPass($username, $password)
    {
        $dbm = new Database();
        $dbm->query("SELECT * FROM stfn_users WHERE stfn_username=:name");
        $dbm->bind("name", $username);
        $result = $dbm->single();
        if($dbm->rowCount() <= 0)
        {
            return FALSE;
        }
        $pvr = password_verify($password, $result->stfn_userpass);
        return $pvr;
    }
    /**
     * Checking Password but login is MD5
     * @param string $username User Name for Login hashed using MD5
     * @param string $password User Password for Login
     * @return bool Is password correct and user exist
     */
    public function checkPassMD5($username, $password)
    {
        $dbm = new Database();
        $dbm->query("SELECT * FROM stfn_users WHERE MD5(stfn_username)=:name");
        $dbm->bind("name", $username);
        $result = $dbm->single();
        if($dbm->rowCount() <= 0)
        {
            return FALSE;
        }
        $pvr = password_verify($password, $result->stfn_userpass);
        return $pvr;
    }
    /**
     * Secret JWT
     */
    const JWT_SECRET = "info@designspektrum.pl|2019|patrykadamczyk@patrykadamczyk.net";
    /**
     * Create JWT
     * @param string $username UserName
     * @return string JWT
     */
    public function jwt_create($username)
    {
        $exp = time() + 3600;
        // $token = \ReallySimpleJWT\Token::create($username, self::JWT_SECRET, $exp, $_SERVER["SERVER_NAME"]);
        // var_dump(get_class_methods((new \Lcobucci\JWT\Builder())));exit;
        $token = (new \Lcobucci\JWT\Builder())
            ->setIssuer($_SERVER["SERVER_NAME"])
            ->setAudience($_SERVER["SERVER_NAME"])
            ->setId(self::JWT_SECRET, true)
            ->setIssuedAt(time())
            ->setExpiration(time()+3600)
            ->set("user_id", $username)
            ->sign((new \Lcobucci\JWT\Signer\Hmac\Sha256()), self::JWT_SECRET)
            ->getToken();
        $token = (string)$token;
        return $token;
    }
    /**
     * Validate JWT
     * @param string $token JWT
     * @return bool Validation Result
     */
    public function jwt_validate($token)
    {
        // return \ReallySimpleJWT\Token::validate($token, self::JWT_SECRET);
        $tokenP = (new \Lcobucci\JWT\Parser())->parse($token);
        $validator = new \Lcobucci\JWT\ValidationData();
        $validator->setIssuer($_SERVER["SERVER_NAME"]);
        $validator->setAudience($_SERVER["SERVER_NAME"]);
        $validator->setId(self::JWT_SECRET);
        return $tokenP->validate($validator) && $tokenP->verify((new \Lcobucci\JWT\Signer\Hmac\Sha256()), self::JWT_SECRET);
    }
    // /**
    //  * Fully Checks JWT
    //  * 1. Checks is JWT Valid
    //  * 2. Checks is User ID in JWT Payload is correct
    //  * 3. Returns Result
    //  * @param string $token JWT
    //  * @return bool Validation Result
    //  */
    // public function check_jwt($token)
    // {
    //     if(\ReallySimpleJWT\Token::validate($token, self::JWT_SECRET))
    //     {
    //         $payload = \ReallySimpleJWT\Token::getPayload($token, self::JWT_SECRET);
    //         $userid = $payload["user_id"];
            
    //         $dbm = new Database();
    //         $dbm->query("SELECT * FROM stfn_users WHERE stfn_username=:name");
    //         $dbm->bind("name", $userid);
    //         $result = $dbm->single();
    //         if($dbm->rowCount() <= 0)
    //         {
    //             return FALSE;
    //         }
    //         return TRUE;
    //     }
    //     else
    //     {
    //         return FALSE;
    //     }
    // }
    /**
     * Login
     * @param string $username User Name
     * @param string $password Password
     * @return string|null Token or Null if login failed
     */
    public function login($username, $password)
    {
        if($this->checkPass($username, $password))
        {
            return $this->jwt_create($username);
        }
        else
        {
            return NULL;
        }
    }
}