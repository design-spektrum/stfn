<?php
/**
 * Icon to HTML
 * @param int $itype Type
 * @param string $iname Name
 * @param string $isrc SRC
 * @return null|string Printed String or NULL on ERROR
 */
function PW__ICON__TYPE2HTML($itype, $iname, $isrc)
{
    switch ($itype)
    {
        case 1:
            $print = '<link rel="'.$iname.'" href="'.$isrc.'"/>';
            echo($print);
            return $print;
        case 2:
            $print = '<meta name="'.$iname.'" content="'.$isrc.'"/>';
            echo($print);
            return $print;
        case 3:
            die("Error: Not Implemented Feature");
            return null;
        default:
            return null;
    }
}
?>
<!DOCTYPE html>
<html lang="pl" <?= (isset($data["html-id"]) ? "id=\"".$data['html-id']."\"" : "") ?> <?= (isset($data["html-class"]) ? "class=\"".$data['html-class']."\"" : "") ?>>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $data['title'] ?></title>
    <?php if(isset($data['author'])): ?>
    <meta name="author" content="<?= $data['author'] ?>"/>
    <meta name="web_author" content="<?= $data['author'] ?>"/>
    <?php endif; ?>
    <?php if(isset($data['contact'])): ?>
    <meta name="contact" content="<?= $data['contact'] ?>"/>
    <?php endif; ?>
    <?php if(isset($data['copyright'])): ?>
    <meta name="copyright" content="<?= $data['copyright'] ?>"/>
    <?php endif; ?>
    <?php if(isset($data['robots'])): ?>
    <meta name="robots" content="<?= $data['robots'] ?>"/>
    <?php endif; ?>
    <?php if(isset($humanstxt)): ?>
    <link type="text/plain" rel="author" href="<?= URLROOT ?>/humans.txt"
    <?php endif; ?>
    <?php if(isset($data['description'])): ?>
    <meta name="description" content="<?= $data['description'] ?>"/>
    <?php endif; ?>
    <?php if(isset($data['keywords'])): ?>
    <meta name="keywords" content="<?= $data['keywords'] ?>"/>
    <?php endif; ?>
    <meta name="application-name" content="STFN"/>
    <?php
    if(isset($data["css-stylesheets-head"]))
    {
        foreach($data["css-stylesheets-head"] as $v)
        {
            print('<link rel="stylesheet" href="'.$v.'"/>');
        }
    }
    ?>
    <link type="text/css" rel="stylesheet" href="<?= URLROOT ?>/assets/app/main.css"/>
</head>
<body <?= (isset($data["body-id"]) ? "id=\"".$data['body-id']."\"" : "") ?> <?= (isset($data["body-class"]) ? "class=\"".$data['body-class']."\"" : "") ?>>
    
<form class="form-signin" method="POST" action="loginform">
    <h1 class="h3 mb-3 font-weight-normal">Panel Administracyjny</h1>
    <?php if(isset($_SESSION["stfn_logerror"])): ?>
    <div class="alert alert-danger" role="alert">
        Błędny login lub hasło!
    </div>
    <?php unset($_SESSION["stfn_logerror"]); ?>
    <?php endif; ?>
    <label for="inputEmail" class="sr-only">Nazwa Użytkownika</label>
    <input type="text" id="inputEmail" class="form-control" name="username" placeholder="Nazwa Użytkownika" autocomplete="username" required autofocus>
    <label for="inputPassword" class="sr-only">Hasło</label>
    <input type="password" id="inputPassword" class="form-control" name="password" placeholder="Hasło" autocomplete="currentpassword" required>
    <button class="btn btn-lg btn-primary btn-block" name="submit" type="submit">Zaloguj</button>
    <p class="mt-5 mb-3 text-muted">DesignSpektrum &copy; 2019<br/>Patryk Adamczyk &copy; 2015 - <?= date("Y") ?></p>
</form>
</body>
<?php
if(isset($data["js-scripts"]))
{
    foreach($data["js-scripts"] as $v)
    {
        print("<script src=\"$v\"></script>");
    }
}
?>
<?php
if(isset($data["js-scripts-code"]))
{
    foreach($data["js-scripts-code"] as $v)
    {
        print("<script>$v</script>");
    }
}
?>
</html>