<!DOCTYPE html>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>STFN</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous"/>
</head>
<body>
    <p class="text-center" style="margin-top: 40vh;">
        Witamy w projekcie tłumaczeń automatycznych.<br/>
        Chcesz się dowiedzieć czegoś więcej?<br/>
        Napisz do zarządców tej instancji: <a href="mailto:info@designspektrum.pl">DesignSpektrum</a>
        <br/>
        Napisz do twórcy projektu: <a href="mailto:patrykadamczyk@patrykadamczyk.net">Patryk Adamczyk @ PAiP Web</a>
    </p>
</body>
</html>