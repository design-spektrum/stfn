function notifyMe() {
  // Sprawdzamy czy przeglądarka obsługuje powiadomienia.
  if (!("Notification" in window)) {
    // alert("Ta przeglądarka nie obsługuje powiadomień");
    return false;
  }

  // Sprawdźmy czy uprawnienia dla powiadomienia zostały nadane
  else if (Notification.permission === "granted") {
    // jeżeli są tworzymy powiadomienie
    // var notification = new Notification("Hi there!");
    return true;
  }

  // W innym przypadku tworzymy zapytanie o uprawnienia
  else if (Notification.permission !== 'denied') {
    Notification.requestPermission(function (permission) {
      //Jeżeli użytkownik zaakceptuje tworzymy powiadomienie
      if (permission === "granted") {
        // var notification = new Notification("Hi there!");
        return true;
      }
    });
  }

  // Na koniec, jeżeli użytkownik odmówił powiadomień i chcesz szanować go
  // nie ma potrzeby dręczyć go zapytaniami
}
notifyMe();


// This function is needed because Chrome doesn't accept a base64 encoded string
// as value for applicationServerKey in pushManager.subscribe yet
// https://bugs.chromium.org/p/chromium/issues/detail?id=802280
function urlBase64ToUint8Array(base64String) {
  var padding = ('=' as any).repeat((4 - base64String.length % 4) % 4);
  var base64 = (base64String + padding)
    .replace(/\-/g, '+')
    .replace(/_/g, '/');
 
  var rawData = window.atob(base64);
  var outputArray = new Uint8Array(rawData.length);
 
  for (var i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
}
const basePath = "assets/app/";
navigator.serviceWorker.register(basePath+'serviceWorker.js');

navigator.serviceWorker.ready
.then(function(registration) {
  return registration.pushManager.getSubscription()
  .then(async function(subscription) {
    if (subscription) {
      return subscription;
    }
    const response = await fetch('./vapidPublicKey');
    const vapidPublicKey = await response.text();
    const convertedVapidKey = urlBase64ToUint8Array(vapidPublicKey);
    return registration.pushManager.subscribe({
      userVisibleOnly: true,
      applicationServerKey: convertedVapidKey
    });
  });
}).then(function(subscription) {
  fetch('./register', {
    method: 'post',
    headers: {
      'Content-type': 'application/json'
    },
    body: JSON.stringify({
      subscription: subscription
    }),
  });

  document.getElementById('doIt').onclick = function() {
    const delay = (document.getElementById('notification-delay') as any).value;
    const ttl = (document.getElementById('notification-ttl') as any).value;
    fetch('./sendNotification', {
      method: 'post',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify({
        subscription: subscription,
        delay: delay,
        ttl: ttl,
      }),
    });
  };

});