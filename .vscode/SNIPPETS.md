# Hmm...
Każdy model powinien mieć:
| L | Action |           Opis           |
| - | ------ | ------------------------ |
| C | Create |       Dodaj Element      |
| R |  Read  |      Pokaż 1 Element     |
| U | Update |     Edytuj 1 Element     |
| D | Delete |      Usuń 1 Element      |
| A |   All  | Pokaż wszystkie elementy |