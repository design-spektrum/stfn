<?php
session_start();
require_once "../app/bootstrap.php";
// PHP Version Checker
require_once "../app/libraries/PHP.php";
PHP::dieByVersion("5.5.0");
PHP::repairPHP();
// Project INI Settings
if(PROJECT_MODE != 2) # This is for easier debugging (For even better use small dataset in database)
{
    # Execution Time more unlimited
    ini_set("max_execution_time", 6000);
    ini_set("max_input_time", 6000);
}
// End of Changes
// Require Bootstraping Script
// Logging on Developer Mode
if(PROJECT_MODE == 2)
{
    // Change Ini Settings
    ini_set("display_errors",1); # Show Errors
    ini_set("display_startup_errors",1); # Show Errors
    error_reporting(E_ALL); # Show All Errors
    ini_set("log_errors", "On"); # Log Errors
    ini_set("error_log", __DIR__."/error_log_php.log"); # Define Location of Log File
    // End of Changes
}
//Errors
set_include_path(get_include_path().";".__DIR__);
// Require Composer Autoload Script
error_reporting(E_ERROR);
require_once "../vendor/autoload.php";
error_reporting(E_ALL);
// Init Core Library
$init = new Core();