-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 22 Sty 2019, 14:28
-- Wersja serwera: 10.1.28-MariaDB
-- Wersja PHP: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `stfn`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `translations`
--

CREATE TABLE `translations` (
  `TID` int(11) NOT NULL COMMENT 'Translation Identifier',
  `T_Phrase` varchar(1000) COLLATE utf8_polish_ci NOT NULL COMMENT 'Translation Phrase',
  `T_Context` varchar(1000) COLLATE utf8_polish_ci DEFAULT NULL COMMENT 'Translation Context',
  `T_Project` varchar(1000) COLLATE utf8_polish_ci NOT NULL DEFAULT 'default' COMMENT 'Translation Project',
  `T_Language` varchar(20) COLLATE utf8_polish_ci NOT NULL DEFAULT '_default_' COMMENT 'Translation Language',
  `Translation` varchar(1000) COLLATE utf8_polish_ci DEFAULT NULL COMMENT 'Translation'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci COMMENT='Translation Model Database Table';

--
-- Wyzwalacze `translations`
--
DELIMITER $$
CREATE TRIGGER `Default_Translation` BEFORE INSERT ON `translations` FOR EACH ROW IF NEW.Translation IS NULL THEN
	SET NEW.Translation := NEW.T_Phrase;
END IF
$$
DELIMITER ;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`TID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `translations`
--
ALTER TABLE `translations`
  MODIFY `TID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Translation Identifier';
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
