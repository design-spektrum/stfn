-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 11 Lut 2019, 14:58
-- Wersja serwera: 10.1.28-MariaDB
-- Wersja PHP: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `stfn`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `stfn_translations`
--

CREATE TABLE `stfn_translations` (
  `TID` int(11) NOT NULL COMMENT 'Translation Identifier',
  `T_Phrase` varchar(1000) COLLATE utf8_polish_ci NOT NULL COMMENT 'Translation Phrase',
  `T_Language` varchar(20) COLLATE utf8_polish_ci NOT NULL COMMENT 'Translation Language',
  `Translation` varchar(1000) COLLATE utf8_polish_ci DEFAULT NULL COMMENT 'Translation'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci COMMENT='Translation Model Database Table';

--
-- Zrzut danych tabeli `stfn_translations`
--

INSERT INTO `stfn_translations` (`TID`, `T_Phrase`, `T_Language`, `Translation`) VALUES
(1, 'test1', '_default_', 'test1'),
(2, 'test2', '_default_', 'test2');

--
-- Wyzwalacze `stfn_translations`
--
DELIMITER $$
CREATE TRIGGER `Default_Translation` BEFORE INSERT ON `stfn_translations` FOR EACH ROW IF NEW.Translation IS NULL THEN
	SET NEW.Translation := NEW.T_Phrase;
END IF
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `stfn_users`
--

CREATE TABLE `stfn_users` (
  `stfn_username` text COLLATE utf8_polish_ci NOT NULL COMMENT 'Nazwa Konta',
  `stfn_userpass` text COLLATE utf8_polish_ci NOT NULL COMMENT 'Hasło Konta'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci COMMENT='Użytkownicy';

--
-- Zrzut danych tabeli `stfn_users`
--

INSERT INTO `stfn_users` (`stfn_username`, `stfn_userpass`) VALUES
('designspektrum', '$2y$10$st7iJvAW9P9.1W2IKUndZezCl5tpFgrffMtymlJR4AaSmyTjQsrvy');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `stfn_translations`
--
ALTER TABLE `stfn_translations`
  ADD PRIMARY KEY (`TID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `stfn_translations`
--
ALTER TABLE `stfn_translations`
  MODIFY `TID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Translation Identifier', AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
