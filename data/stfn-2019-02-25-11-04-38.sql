-- Adminer 4.7.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `stfn_notifications`;
CREATE TABLE `stfn_notifications` (
  `NID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Notification Identifier',
  `N_Context` varchar(10000) COLLATE utf8_polish_ci DEFAULT NULL COMMENT 'Notification Context',
  `N_Title` varchar(1000) COLLATE utf8_polish_ci NOT NULL COMMENT 'Notification Title',
  `N_Content` varchar(10000) COLLATE utf8_polish_ci NOT NULL COMMENT 'Notification Content',
  `N_Date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Notification Creation Date',
  PRIMARY KEY (`NID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci COMMENT='Notifications';

TRUNCATE `stfn_notifications`;

DROP TABLE IF EXISTS `stfn_translations`;
CREATE TABLE `stfn_translations` (
  `TID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Translation Identifier',
  `T_Phrase` varchar(1000) COLLATE utf8_polish_ci NOT NULL COMMENT 'Translation Phrase',
  `T_Language` varchar(20) COLLATE utf8_polish_ci NOT NULL COMMENT 'Translation Language',
  `Translation` varchar(1000) COLLATE utf8_polish_ci DEFAULT NULL COMMENT 'Translation',
  PRIMARY KEY (`TID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci COMMENT='Translation Model Database Table';

TRUNCATE `stfn_translations`;
INSERT INTO `stfn_translations` (`TID`, `T_Phrase`, `T_Language`, `Translation`) VALUES
(1,	'test1',	'_default_',	'test1'),
(2,	'test2',	'_default_',	'test2'),
(3,	'testing-stfn',	'pl',	'STFN is the best'),
(4,	'testing-stfn2',	'pl',	'Is this working?'),
(5,	'test5',	'pl',	'test5'),
(6,	'Demo sklepu designspektrum',	'pl',	'Demo sklepu designspektrum'),
(7,	'test221',	'pl',	'coś tam'),
(8,	'test2213',	'pl',	'NULL'),
(9,	'test22231',	'pl',	'test22231'),
(10,	'test22222231',	'pl',	'test22222231'),
(11,	'test2222226631',	'pl',	'test2222226631'),
(12,	'test22222226631',	'pl',	NULL),
(13,	'test22222226631',	'pl',	NULL),
(14,	'test22222226631',	'pl',	NULL);

DELIMITER ;;

CREATE TRIGGER `Default_Translation` BEFORE INSERT ON `stfn_translations` FOR EACH ROW
IF NEW.Translation IS NULL THEN
	#SET NEW.Translation := NEW.T_Phrase;
	SET NEW.Translation := NULL;
END IF;;

DELIMITER ;

DROP TABLE IF EXISTS `stfn_users`;
CREATE TABLE `stfn_users` (
  `stfn_username` text COLLATE utf8_polish_ci NOT NULL COMMENT 'Nazwa Konta',
  `stfn_userpass` text COLLATE utf8_polish_ci NOT NULL COMMENT 'Hasło Konta'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci COMMENT='Użytkownicy';

TRUNCATE `stfn_users`;
INSERT INTO `stfn_users` (`stfn_username`, `stfn_userpass`) VALUES
('designspektrum',	'$2y$10$st7iJvAW9P9.1W2IKUndZezCl5tpFgrffMtymlJR4AaSmyTjQsrvy');

-- 2019-02-25 10:04:38
