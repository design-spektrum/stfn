-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 28 Lut 2019, 10:12
-- Wersja serwera: 10.1.28-MariaDB
-- Wersja PHP: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `stfn`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `stfn_notifications`
--

CREATE TABLE `stfn_notifications` (
  `NID` int(11) NOT NULL COMMENT 'Notification Identifier',
  `N_Context` varchar(10000) COLLATE utf8_polish_ci DEFAULT NULL COMMENT 'Notification Context',
  `N_Title` varchar(1000) COLLATE utf8_polish_ci NOT NULL COMMENT 'Notification Title',
  `N_Content` varchar(10000) COLLATE utf8_polish_ci NOT NULL COMMENT 'Notification Content',
  `N_Date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Notification Creation Date'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci COMMENT='Notifications';

--
-- Zrzut danych tabeli `stfn_notifications`
--

INSERT INTO `stfn_notifications` (`NID`, `N_Context`, `N_Title`, `N_Content`, `N_Date`) VALUES
(1, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Fraza:, której nie było w systemie.', '0000-00-00 00:00:00'),
(2, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Szukaj, której nie było w systemie.', '0000-00-00 00:00:00'),
(3, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Zdjęcie [intro], której nie było w systemie.', '0000-00-00 00:00:00'),
(4, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Zdjęcia [lista], której nie było w systemie.', '0000-00-00 00:00:00'),
(5, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy LP, której nie było w systemie.', '0000-00-00 00:00:00'),
(6, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Plik, której nie było w systemie.', '0000-00-00 00:00:00'),
(7, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Multimedia, której nie było w systemie.', '0000-00-00 00:00:00'),
(8, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Alt, której nie było w systemie.', '0000-00-00 00:00:00'),
(9, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Alt (en), której nie było w systemie.', '0000-00-00 00:00:00'),
(10, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Title, której nie było w systemie.', '0000-00-00 00:00:00'),
(11, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Title (en), której nie było w systemie.', '0000-00-00 00:00:00'),
(12, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Tytuł, której nie było w systemie.', '0000-00-00 00:00:00'),
(13, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Tytuł (en), której nie było w systemie.', '0000-00-00 00:00:00'),
(14, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Opis, której nie było w systemie.', '0000-00-00 00:00:00'),
(15, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Opis (en), której nie było w systemie.', '0000-00-00 00:00:00'),
(16, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Data, której nie było w systemie.', '0000-00-00 00:00:00'),
(17, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Data (en), której nie było w systemie.', '0000-00-00 00:00:00'),
(18, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Url, której nie było w systemie.', '0000-00-00 00:00:00'),
(19, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Url (en), której nie było w systemie.', '0000-00-00 00:00:00'),
(20, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Wyslij, której nie było w systemie.', '0000-00-00 00:00:00'),
(21, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Design Spektrum. Profesjonalne tworzenie e-sklepów. Kontakt., której nie było w systemie.', '0000-00-00 00:00:00'),
(22, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Kategoria:, której nie było w systemie.', '0000-00-00 00:00:00'),
(23, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Lp:, której nie było w systemie.', '0000-00-00 00:00:00'),
(24, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Nazwa:, której nie było w systemie.', '0000-00-00 00:00:00'),
(25, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Nazwa: (en), której nie było w systemie.', '0000-00-00 00:00:00'),
(26, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Tytuł:, której nie było w systemie.', '0000-00-00 00:00:00'),
(27, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Tytuł: (en), której nie było w systemie.', '0000-00-00 00:00:00'),
(28, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Widoczność:, której nie było w systemie.', '0000-00-00 00:00:00'),
(29, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Widoczność: (en), której nie było w systemie.', '0000-00-00 00:00:00'),
(30, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy menu górne:, której nie było w systemie.', '0000-00-00 00:00:00'),
(31, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy menu górne: (en), której nie było w systemie.', '0000-00-00 00:00:00'),
(32, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy menu dolne:, której nie było w systemie.', '0000-00-00 00:00:00'),
(33, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy menu dolne: (en), której nie było w systemie.', '0000-00-00 00:00:00'),
(34, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Widoczność na stronie głównej:, której nie było w systemie.', '0000-00-00 00:00:00'),
(35, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Widoczność na stronie głównej: (en), której nie było w systemie.', '0000-00-00 00:00:00'),
(36, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Formularz kontaktowy:, której nie było w systemie.', '0000-00-00 00:00:00'),
(37, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Formularz kontaktowy: (en), której nie było w systemie.', '0000-00-00 00:00:00'),
(38, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Intro:, której nie było w systemie.', '0000-00-00 00:00:00'),
(39, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Intro: (en), której nie było w systemie.', '0000-00-00 00:00:00'),
(40, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Tekst:, której nie było w systemie.', '0000-00-00 00:00:00'),
(41, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Tekst: (en), której nie było w systemie.', '0000-00-00 00:00:00'),
(42, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Url:, której nie było w systemie.', '0000-00-00 00:00:00'),
(43, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Url: (en), której nie było w systemie.', '0000-00-00 00:00:00'),
(44, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Opis:, której nie było w systemie.', '0000-00-00 00:00:00'),
(45, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Opis: (en), której nie było w systemie.', '0000-00-00 00:00:00'),
(46, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Słowa kluczowe:, której nie było w systemie.', '0000-00-00 00:00:00'),
(47, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Słowa kluczowe: (en), której nie było w systemie.', '0000-00-00 00:00:00'),
(48, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Zapisz, której nie było w systemie.', '0000-00-00 00:00:00'),
(49, 'Język nie został podany!', 'Błąd zapytania do API!', 'Serwer 192.168.1.199:90 utworzył nie prawidłowe zapytanie do API. Sprawdź czy obsługa dostępu do api została zrobiona poprawnie lub czy nie zmieniałeś systemu API.', '0000-00-00 00:00:00'),
(50, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Demo sklepu designspektrum, której nie było w systemie.', '0000-00-00 00:00:00'),
(51, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Słowa kluczowe:, której nie było w systemie.', '0000-00-00 00:00:00'),
(52, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Słowa kluczowe: (en), której nie było w systemie.', '0000-00-00 00:00:00'),
(53, 'Fraza nie znaleziona lub nie przetłumaczona!', 'Fraza nie została przetłumaczona!', 'Serwer 192.168.1.199:90 prosił o przetłumaczenie frazy Design Spektrum. Profesjonalne tworzenie e-sklepów. Kontakt., której nie było w systemie.', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `stfn_translations`
--

CREATE TABLE `stfn_translations` (
  `TID` int(11) NOT NULL COMMENT 'Translation Identifier',
  `T_Phrase` varchar(1000) COLLATE utf8_polish_ci NOT NULL COMMENT 'Translation Phrase',
  `T_Language` varchar(20) COLLATE utf8_polish_ci NOT NULL COMMENT 'Translation Language',
  `Translation` varchar(1000) COLLATE utf8_polish_ci DEFAULT NULL COMMENT 'Translation'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci COMMENT='Translation Model Database Table';

--
-- Zrzut danych tabeli `stfn_translations`
--

INSERT INTO `stfn_translations` (`TID`, `T_Phrase`, `T_Language`, `Translation`) VALUES
(72, 'testTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT', 'pl', 'testtestttt'),
(74, 'test', 'pl', 'testttttttttt238721932193'),
(75, 'test2222', 'pl', NULL),
(76, 'test', 'test', 'test'),
(77, 'test', 'test', 'test'),
(78, 'test', 'test', 'test'),
(79, 'Demo sklepu designspektrum', 'pl', NULL),
(80, 'Słowa kluczowe:', 'pl', NULL),
(81, 'Słowa kluczowe: (en)', 'pl', NULL),
(82, 'Design Spektrum. Profesjonalne tworzenie e-sklepów. Kontakt.', 'pl', NULL);

--
-- Wyzwalacze `stfn_translations`
--
DELIMITER $$
CREATE TRIGGER `Default_Translation` BEFORE INSERT ON `stfn_translations` FOR EACH ROW IF NEW.Translation IS NULL THEN
	#SET NEW.Translation := NEW.T_Phrase;
	SET NEW.Translation := NULL;
END IF
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `stfn_users`
--

CREATE TABLE `stfn_users` (
  `stfn_username` text COLLATE utf8_polish_ci NOT NULL COMMENT 'Nazwa Konta',
  `stfn_userpass` text COLLATE utf8_polish_ci NOT NULL COMMENT 'Hasło Konta'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci COMMENT='Użytkownicy';

--
-- Zrzut danych tabeli `stfn_users`
--

INSERT INTO `stfn_users` (`stfn_username`, `stfn_userpass`) VALUES
('designspektrum', '$2y$10$st7iJvAW9P9.1W2IKUndZezCl5tpFgrffMtymlJR4AaSmyTjQsrvy');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `stfn_notifications`
--
ALTER TABLE `stfn_notifications`
  ADD PRIMARY KEY (`NID`);

--
-- Indexes for table `stfn_translations`
--
ALTER TABLE `stfn_translations`
  ADD PRIMARY KEY (`TID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `stfn_notifications`
--
ALTER TABLE `stfn_notifications`
  MODIFY `NID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Notification Identifier', AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT dla tabeli `stfn_translations`
--
ALTER TABLE `stfn_translations`
  MODIFY `TID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Translation Identifier', AUTO_INCREMENT=83;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
