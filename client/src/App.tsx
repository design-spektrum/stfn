import React, { Component } from 'react';
import { HashRouter as Router, Route, Switch } from "react-router-dom";

import { Provider } from 'react-redux';
import store from './store';

import Index from './components/pages/Index';
import Tester from './components/pages/Tester';
import Translations from './components/pages/Translations';
import Notifications from './components/pages/Notifications';
import AddTranslation from './components/pages/AddTranslation';
import EditTranslation from './components/pages/EditTranslation';
import Docs from './docs/Index';

import Page from './components/lib/Page';

import './styles/main.scss';
import config from './config';
import axios from "axios";
import {authenticate} from './actions/authAction';

if (localStorage["jwt"]) {
  //  Set auth token auth
  authenticate();
  axios.defaults.headers.common['Authorization'] = localStorage["jwt"]; // for all requests
  config.API.defaults.headers.common['Authorization'] = localStorage["jwt"]; // for all requests
}
class App extends Component {
  render() {
    // config.API.get("/RA9_PW_Tools/test/").then(console.warn).catch(console.warn);
    return (
      <Provider store={store}>
        <Router>
          <Switch>
            <Route exact path="/" component={Index} />
            <Route exact path="/tester" component={Tester} />
            <Route exact path="/translations" component={Translations} />
            <Route exact path="/translationadd" component={AddTranslation} />
            <Route exact path="/translationedit/:tid" component={EditTranslation} />
            <Route exact path="/docs" component={Docs} />
            <Route exact path="/notifications" component={Notifications} />
            <Route component={this.NotFound} />
          </Switch>
        </Router>
      </Provider>
    );
  }
  NotFound() {
    return (
      <Page>
        <div className="HTTP404">
          <h2 className="w-100 text-center">
            Strony nieznaleziono
          </h2>
          <div className="w-100 text-center" style={{
            marginTop: "20px",
            marginBottom: "20px"
          }}>
            Przepraszamy za niedogodności ale ta strona nie istnieje.
          </div>
          <div className="w-100 text-center text-muted" style={{
            marginTop: "20px"
          }}>
            HTTP Error 404
          </div>
        </div>
      </Page>
    )
  }
}

export default App;
