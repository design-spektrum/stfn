import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { getStatistics } from '../actions/statisticsActions';
import { translate } from '../lib/lang';
import config from '../config';
// import Test from '../pwm/components/DateTime';

export class StatisticsPage extends Component<any, any> {
  static propTypes = {
    statistics: PropTypes.object.isRequired,
    getStatistics: PropTypes.func.isRequired
  }
  componentDidMount()
  {
    this.props.getStatistics();
  }
  render() {
    return (
      <div className="row">
        {/* <Test onChange={(e, val) => console.log(e,val)}/> */}
        <div className="col-lg-6 col-md-6">
          <div className="list-group">
            <h3 className="list-group-header">Informacje o Panelu Administracyjnym</h3>
            {Object.keys(this.props.statistics.frontend).map((key, i) => (
              <span className="list-group-item stat" key={i}>
                <span>{translate(`stats${config.translation_separator}${key}`)}:&nbsp;
                  <span>
                    {translate(`stats${config.translation_separator}${key}${config.translation_separator}${this.props.statistics.frontend[key]}`)}
                  </span>
                </span>
              </span>
            ))}
          </div>
        </div>
        <div className="col-lg-6 col-md-6">
          <div className="list-group">
            <h3 className="list-group-header">Informacje o API</h3>
            {Object.keys(this.props.statistics.backend).map((key, i) => (
              <span className="list-group-item stat" key={i}>
                <span>{translate(`stats${config.translation_separator}${key}`)}:&nbsp;
                  <span>
                  {translate(`stats${config.translation_separator}${key}${config.translation_separator}${this.props.statistics.backend[key]}`)}  </span>
                </span>
              </span>
            ))}
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  statistics: state.statistics.statistics
})

export default connect(mapStateToProps, {getStatistics})(StatisticsPage)
