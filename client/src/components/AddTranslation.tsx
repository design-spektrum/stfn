import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { addTranslations } from '../actions/translationsActions'
import TextInputGroup from './libcomponents/TextInputGroup'
import { NavLink as Link } from "react-router-dom"

interface TranslationState
{
  errors: any
  form: {
    phrase: string
    lang: string
    translation: string
  }
}

class AddTranslation extends Component<any,any> {
  static propTypes = {
    translations: PropTypes.array,
    addTranslations: PropTypes.func.isRequired
  }
  state: TranslationState = {
    errors: {
    },
    form: {
      phrase: "",
      lang: "",
      translation: ""
    }
  }
  constructor(props: any)
  {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
    // this.translations = [];
  }
  onSubmit(e): void
  {
		// Don't submit form like it would be simple boring form.
    e.preventDefault();
    let data = {...this.state.form};
    if(this.state.form.translation === "")
    {
      data.translation = null;
    }
    this.props.addTranslations(data);
    // alert("Dodano frazę!");
  }
  onChange(e): void
  {
		this.setState({form: { ...this.state.form, [e.target.name]: e.target.value }});
  }
  render() {
    const {phrase, lang, translation} = this.state.form;
    return (
      <div>
						<div className="card mb-3">
							<div className="card-header">Dodaj Tłumaczenie</div>
							<div className="card-body">
								<form onSubmit={this.onSubmit} className="row">
									<TextInputGroup
										name="lang"
										label="Język"
										placeholder="Język"
										value={lang}
										type="text"
										onChange={this.onChange}
										error={this.state.errors.lang}
                    classes="col-12 col-sm-4 col-md-2"
									/>
									<TextInputGroup
										name="phrase"
										label="Fraza"
										placeholder="Podaj frazę do przetłumaczenia"
										value={phrase}
										type="text"
										onChange={this.onChange}
                    error={this.state.errors.phrase}
                    classes="col-12 col-sm-8 col-md-10"
									/>
									<TextInputGroup
										name="translation"
										label="Tłumaczenie"
										placeholder="Podaj tłumaczenie"
										value={translation}
										type="text"
										onChange={this.onChange}
                    error={this.state.errors.translation}
                    classes="col-12"
									/>
									{this.state.errors.general && (
										<div className="w-100 alert alert-danger">
											{this.state.errors.general}
										</div>
									)}
									<input
										type="submit"
										value="Dodaj"
										className="btn btn-success btn-block"
									/>
								</form>
							</div>
              <div className="card-footer">
                <Link to="/translations" className="btn btn-danger btn-block" activeClassName="active">
                  Powrót
                </Link>
              </div>
						</div>
      </div>
    )
  }
}


const mapStateToProps = (state) => ({
})
export default connect(mapStateToProps, {addTranslations})(AddTranslation);
