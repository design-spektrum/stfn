import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { getTranslations, removeTranslation } from '../actions/translationsActions';
import { translate } from '../lib/lang';
// import getFlag from '../lib/flags';
import config from '../config';
import Search from '../pwm/components/Search';
import Filters from './libcomponents/Filters';
import {sleep} from '../lib/functions';
import { NavLink as Link } from "react-router-dom";

export class TranslationPage extends Component<any, any> {
  static propTypes = {
    translations: PropTypes.array,
    getTranslations: PropTypes.func.isRequired
  }
  translations: any[];
  state = {
    translations: [],
    translation: [],
    searchres: [],
    search: false,
    filtered: false,
    changed: false
  }
  static defaultProps = {
    translations: []
  };
  constructor(props: any)
  {
    super(props);
    this.updateProps = this.updateProps.bind(this);
    this.onSearch = this.onSearch.bind(this);
    this.onFilter = this.onFilter.bind(this);
    this.onDelete = this.onDelete.bind(this);
    // this.translations = [];
  }
  updateProps(base=0)
  {
    sleep(base+200).then(() => {
      this.props.getTranslations();
      sleep(500).then(() => {
        this.setState({
          translations: this.props.translations
        });
      });
    });
  }
  componentDidMount()
  {
    this.props.getTranslations();
    // this.translations = this.props.translations;
    this.setState({
      translations: this.props.translations
    });
    this.updateProps();
  }
  onSearch(res: any[]): void
  {
    // console.log(res);
    let tmp: any[];
    if(this.state.filtered)
    {
      tmp = this.state.translation.filter((val) => res.indexOf(val.phrase) !== -1);
    }
    else
    {
      tmp = this.props.translations.filter((val) => res.indexOf(val.phrase) !== -1);
    }
    this.setState({
      ...this.state,
      search: true,
      searchres: res,
      translations: tmp
    });
  }
  onFilter(res: any[]): void
  {
    // console.log(res);
    // let tmp: any[] = this.props.translations.filter((val) => res.indexOf(val.phrase) === -1);
    let tmp: any[];
    if(this.state.search)
    {
      tmp = res.filter((val) => this.state.searchres.indexOf(val.phrase) !== -1);
    }
    else
    {
      tmp = res;
    }
    this.setState({
      ...this.state,
      filtered: true,
      translation: res,
      translations: tmp
    });
  }
  onDelete(e): void
  {
    const tid = e.target.getAttribute("data-tid");
    this.props.removeTranslation(tid, () => {
      this.updateProps(-100);
      alert("Usunięto tłumaczenie!");
    });
  }
  render() {
    let t: any[] = [];
    let lang: any[] = [];
    if(this.state.translations.length === 0 && !this.state.changed)
    {
      sleep(500).then(() => {
        this.setState({
          ...this.state,
          changed: true,
          translations: this.props.translations
        });
      });
    }
    if(this.props.translations !== undefined){
      this.props.translations.map((val) => {
        t.push(val.phrase);
        if(lang.indexOf(val.lang) === -1)
        {
          lang.push(val.lang);
        }
      });
    }
    const debug = false;
    return (
      <div className="">
        <h3 className="">Translacje</h3>
        {/* <Search subject={t} debug={true} onFind={(m) => {console.log(m)}}/> */}
        <div className="">
          <div className="input-group mb-3">
            <div className="input-group-prepend">
              <span className="input-group-text" id="phrase-search">Fraza</span>
            </div>
            <Search subject={t} debug={debug} classes="form-control" placeholder="Fraza" aria-label="Fraza" aria-describedby="phrase-search" onFind={this.onSearch}/>
            {/* <input type="text" className="form-control" placeholder="Fraza" aria-label="Fraza" aria-describedby="phrase-search" onChange={this.searcherChange}/> */}
          </div>
          <div className="input-group mb-3">
            <div className="input-group-prepend">
              <span className="input-group-text" id="filter-search">Filtry</span>
            </div>
            <Filters subject={lang} fullsubject={this.props.translations} debug={debug} classes="form-control" placeholder="Filtry" aria-label="Filtry" aria-describedby="filter-search" onFind={this.onFilter}/>
            {/* <input type="text" className="form-control" placeholder="Fraza" aria-label="Fraza" aria-describedby="phrase-search" onChange={this.searcherChange}/> */}
          </div>
        </div>
        <table className="table table-hover w-100">
          <thead>
            <tr className="row">
              <th className="col-md-1" scope="col">
                TID
              </th>
              <th className="col-md-1" scope="col">
                Język
              </th>
              <th className="col-md-4" scope="col">
                Fraza
              </th>
              <th className="col-md-5" scope="col">
                Translacja
              </th>
              <th className="col-md-1" scope="col">
                Akcje
              </th>
            </tr>
          </thead>
          <tbody>
            {this.state.translations !== null && this.state.translations !== undefined ? this.state.translations.map((t) => {
              // console.log(t);
              return (
              <tr key={t.tid} className="row">
                <th className="col-md-1" scope="row">
                  {t.tid}
                </th>
                <td className="col-md-1">
                  {t.lang}
                </td>
                <td className="col-md-4">
                  {t.phrase}
                </td>
                <td className="col-md-5">
                  {t.translation}
                </td>
                <td className="col-md-1">
                  <Link to={`/translationedit/${t.tid}`} exact>
                    <button className="btn btn-warning" data-tid={t.tid}>
                      <i className="fas fa-pencil-alt"/>
                    </button>
                  </Link>
                  &nbsp;
                  <button className="btn btn-danger" data-tid={t.tid} onClick={this.onDelete}>
                    <i className="fas fa-times"/>
                  </button>
                </td>
              </tr>
            )}) : (<tr><td colSpan={5} className="text-center">{this.state.translations===null ? "Brak wyników" : "Błąd"}</td></tr>)}
            {this.state.translations.length === 0 ? (<tr><td colSpan={5} className="text-center">Brak wyników</td></tr>) : (<tr style={{display: "none"}}></tr>)}
          </tbody>
        </table>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  translations: state.translation.translations
})

export default connect(mapStateToProps, {getTranslations, removeTranslation})(TranslationPage)
