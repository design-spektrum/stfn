import React, { Component } from 'react';
import PropTypes from 'prop-types';

/**
 * Filter Component
 * Properties:
 * * classes - CSS Classes of Input for Searcher
 * * subject - Array to search from
 * * onFind(RESULTS) - Function that will receive results of search
 * * debug - Debug Flag - When enabled you will see in console some debug logs
 * * placeholder - Placeholder for input
 * * aria-label - Aria-Label for input
 * * aria-describedby - Aria-Describedby
 */
export default class Filters extends Component<any, any> {
  /**
   * Properties Types
   */
  static propTypes = {
    classes: PropTypes.string,
    subject: PropTypes.array.isRequired,
    fullsubject: PropTypes.array.isRequired,
    onFind: PropTypes.func.isRequired,
    debug: PropTypes.bool,
    placeholder: PropTypes.string,
    'aria-label': PropTypes.string,
    'aria-describedby': PropTypes.string
  }
  /**
   * Default Properties
   */
  static defaultProps = {
    debug: false
  };
  /**
   * Constructor of Component
   */
  constructor(props)
  {
    super(props);
    this.onChange = this.onChange.bind(this);
  }
  /**
   * Logging if debug in props is true
   * @param text Text to log
   * @param t Type [1 - group, 2 - groupend, 0/def - log]
   * @returns console on def/0
   */
  log(text: string = "", t: number = 0): any
  {
    if(this.props.debug)
    {
      switch(t)
      {
        case 1:
          console.group(text);
          break;
        case 2:
          console.groupEnd();
          break;
        case 0:
        default:
          return console.log;
      }
    }
    else
    {
      return function(){};
    }
  }
  /**
   * On Change Event Handler
   * @param e Event Object
   */
  onChange(e: any): void
  {
    this.log("Filter Component Debug", 1);
    this.log()("Subject", this.props.fullsubject);
    this.log()("Input Change occured", e);
    const search = e.target.value;
    this.log()("Filter Found", search);
    let results: any[];
    switch(search)
    {
      case "F_ALL":
        // Finds all untranslated
        this.log()("Filtering by: All");
        results = this.props.fullsubject;
        this.log()("Filtered", results);
        break;
      case "F_untranslated":
        // Finds all untranslated
        this.log()("Filtering by: All untranslated");
        results = this.props.fullsubject.filter((val) => val.translation === null);
        this.log()("Filtered", results);
        break;
      case "F_puntranslated":
        // Finds all propably untranslated
        this.log()("Filtering by: All probably untranslated");
        results = this.props.fullsubject.filter((val) => val.translation === val.phrase);
        this.log()("Filtered", results);
        break;
      case "F_translated":
        // Finds all translated (and probably untranslated)
        this.log()("Filtering by: All translated");
        results = this.props.fullsubject.filter((val) => val.translation !== null);
        this.log()("Filtered", results);
        break;
      default:
        // Language
        this.log()(`Filtering by: All that have ${search} as language`);
        results =  this.props.fullsubject.filter((val) => val.lang === search);
        this.log()("Filtered", results);
        break;
    }
    this.log()("Searching complete", results);
    this.log()("onFind - called")
    this.props.onFind(results);
    this.log("", 2);
  }
  /**
   * Render Method
   */
  render() {
    // <input className={this.props.classes} placeholder={this.props.placeholder} aria-label={this.props['aria-label']} aria-describedby={this.props['aria-describedby']} onChange={this.onChange}/>
    return (
      <select className={this.props.classes} placeholder={this.props.placeholder} aria-label={this.props['aria-label']} aria-describedby={this.props['aria-describedby']} onChange={this.onChange}>
        <option value="F_ALL" defaultChecked>Wszystkie</option>
        <optgroup label="Wyszukiwanie po właściwości">
          <option value="F_untranslated">Nieprzetłumaczone</option>
          <option value="F_puntranslated">Możliwe że nie przetłumaczone</option>
          <option value="F_translated">Przetłumaczone</option>
        </optgroup>
        <optgroup label="Wyszukiwanie po języku">
          {this.props.subject.map((val) => (
            <option key={val} value={val}>Język: {val}</option>
          ))}
        </optgroup>
      </select>
    )
  }
}
