import React, { Component } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

export default class TextInputGroup extends Component<any,any> {
  static propTypes = {
    label: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    placeholder: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    error: PropTypes.string,
    classes: PropTypes.string
  };
  static defaultProps = {
    type: "text"
  };
  render() {
    const classes = this.props.classes !== undefined ? this.props.classes : "";
    return (
      <div className={classnames("form-group", classes)}>
        <label htmlFor={this.props.name}>{this.props.label}</label>
        <input
          type={this.props.type}
          name={this.props.name}
          className={classnames("form-control form-control-lg", {
            "is-invalid": this.props.error
          })}
          placeholder={this.props.placeholder}
          value={this.props.value}
          onChange={this.props.onChange}
        />
        {this.props.error && <div className="invalid-feedback">{this.props.error}</div>}
      </div>
    )
  }
}
