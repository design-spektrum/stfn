import React, { Component } from 'react'
import PropTypes from 'prop-types';

export default class InputGroup extends Component<any, any> {
  static propTypes = {
    id: PropTypes.string.isRequired,
    label: PropTypes.any.isRequired,
  }
  render() {
    return (
        <div className="input-group mb-3">
            <div className="input-group-prepend">
              <span className="input-group-text" id={this.props.id}>{this.props.label}</span>
            </div>
            {this.props.children}
        </div>
    )
  }
}