import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { getNotifications } from '../actions/notificationsActions';
import { translate } from '../lib/lang';
// import getFlag from '../lib/flags';
import config from '../config';
import {sleep} from '../lib/functions';
import { NavLink as Link } from "react-router-dom";

export class NotificationsPage extends Component<any, any> {
  static propTypes = {
    notifications: PropTypes.array,
    getNotifications: PropTypes.func.isRequired
  }
  static defaultProps = {
    notifications: []
  };
  componentDidMount()
  {
    this.props.getNotifications();
  }
  render() {
    console.log(this.props.notifications);
    return (
      <div className="">
        <h3 className="">Powiadomienia</h3>
        <div className="alert alert-danger text-center">Ta funkcjonalność jest w budowie!</div>
        <div className="alerts">
          {this.props.notifications.map((t) => {
            return (
              <div className="alert alert-primary" role="alert">
                <h4 className="alert-heading">{t.title}</h4>
                <hr/>
                <p>{t.content}</p>
                <hr/>
                <div className="alert-footer mb-5">
                  <p className="text-muted float-left">{t.context}</p>
                  <p className="text-muted float-right">{t.date}</p>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  notifications: state.notification.notifications
})

export default connect(mapStateToProps, {getNotifications})(NotificationsPage)
