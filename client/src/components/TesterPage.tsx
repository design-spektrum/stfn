import React, { Component } from 'react';
import InputGroup from './libcomponents/InputGroup';
import axios from "axios";
import { fullAPIURL } from '../lib/functions';
import config from '../config';

export default class TesterPage extends Component<any, any> {
  constructor(props)
  {
    super(props);
    this.state = {
      apiversion: 'latest',
      route: '',
      output: `{"Tutaj będzie wynik zapytania": true}`
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onChange_Templates = this.onChange_Templates.bind(this);
  }
  onSubmit(event: any): void
  {
    event.preventDefault();
    const debug = false;
    if(debug)
    {
      const url = fullAPIURL(this.state.apiversion, this.state.route);
      axios
        .get(url)
        .then(
          res => {
            this.setState({
              ...this.state,
              output: JSON.stringify(res.data, null, 2)
            });
          }
        )
        .catch(
          e => {
            alert("Twoje żądanie do API było nie poprawne");
            console.error(e);
          }
        )
    }
    else
    {
      config.API
        .get(this.state.route)
        .then(
          res => {
            this.setState({
              ...this.state,
              output: JSON.stringify(res.data, null, 2)
            });
          }
        )
        .catch(
          e => {
            alert("Twoje żądanie do API było nie poprawne");
            console.error(e);
          }
        )
    }
  }
  onChange(event: any): void
  {
    this.setState({ [event.target.name]: event.target.value });
  }
  onChange_Templates(event: any): void
  {
    const sourceValue = event.target.value;
    const targetName = event.target.getAttribute('data-target');
    const targetElement = event.target.parentElement.querySelector(`[name=${targetName}]`);
    targetElement.value = sourceValue;
    this.setState({
      route: sourceValue
    });
  }
  render() {
    return (
      <div>
        <h2>Tester API</h2>
        <form onSubmit={this.onSubmit}>
          {/* <label>Wersja API: 
            <select>
              <option value="dev">Wersja Deweloperska</option>
            </select>
          </label>
          <br/>
          Tutaj będzie tester.
          <input type="submit" value="Test"/> */}
          {/* <div className="list-group">
            <h3 className="list-group-header">Trasa API</h3>
              <span className="list-group-item">
              </span>
          </div> */}
          <div className="list-group">
            <h4 className="list-group-header">Trasa API</h4>
              <span className="list-group-item">
                <InputGroup id="apiversion" label="Wersja API">
                  <select className="form-control" aria-label="Wersja API" aria-describedby="apiversion" name="apiversion" onChange={this.onChange} value={this.state.apiversion}>
                    <option value="latest">Najnowsza Wersja</option>
                    <option value="dev">Wersja Deweloperska</option>
                    <option value="1">Wersja 1</option>
                  </select>
                </InputGroup>
              </span>
              <span className="list-group-item">
                <InputGroup id="route" label="Trasa">
                  <input type="text" className="form-control" placeholder="Podaj trasę którą chcesz sprawdzić" aria-label="Trasa" aria-describedby="route"  name="route" onChange={this.onChange} value={this.state.route} required/>
                  <select className="form-control" aria-label="Trasa" aria-describedby="route" data-target="route" onChange={this.onChange_Templates}>
                    <option value="">Twoja Trasa</option>
                    <optgroup label="Publiczne API">
                      <option value="SettingsAPI/getsettings">Ustawienia API</option>
                      <option value="translate/:JĘZYK/:FRAZA">Konkretna translacja</option>
                    </optgroup>
                    <optgroup label="API Administracyjne">
                      <option value="api/translation/?tid=:TID">Konkretna translacja TID</option>
                      <option value="api/translation/?lang=:LANG&phrase=:PHRASE">Konkretna translacja Fraza+Język</option>
                      <option value="api/translations/">Wszystkie translacje</option>
                      <option value="api/notifications/">Wszystkie Powiadomienia</option>
                    </optgroup>
                    <optgroup label="Narzędzia Dodatkowe">
                      <option value="RA9_PW_Tools/password/:PASSWORD">Zahaszuj hasło</option>
                      <option value="RA9_PW_Tools/generate_version/:VERSION">Wygeneruj stałe nowej wersji</option>
                    </optgroup>
                  </select>
                </InputGroup>
                <div className="w-100 text-center text-muted">
                  Uwaga! To nie są wszystkie dostępne trasy. Niektóre trasy są do CRUD. Tutaj zamieszczane są te odpowiadające za dostanie konkretnych rzeczy.
                </div>
              </span>
          </div>
          <div className="list-group">
            <h4 className="list-group-header">Testuj</h4>
              <span className="list-group-item">
                <button type="submit" className="btn btn-primary w-100">
                  Testuj
                </button>
              </span>
          </div>
          <div className="list-group">
            <h4 className="list-group-header">Wynik</h4>
            <span className="list-group-item">
              <pre>
                <code id="tester-output">
                  {this.state.output}
                </code>
              </pre>
            </span>
          </div>
        </form>
      </div>
    )
  }
}
