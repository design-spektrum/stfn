import React, { Component } from 'react'
import Header from './Header'
import Footer from './Footer'

export default class Page extends Component<any, any> {
  render() {
    return (
        <div className="container-fluid">
            <Header/>
            <div className="page-content" style={{
              marginTop: "60px",
              marginBottom: "60px"
            }}>
              {this.props.children}
            </div>
            <Footer/>
        </div>
    )
  }
}
