import React, { Component } from 'react'
import config from '../../config';

import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { getStatistics } from '../../actions/statisticsActions';
//export default 
class Footer extends Component<any, any> {
  static propTypes = {
    statistics: PropTypes.object.isRequired,
    getStatistics: PropTypes.func.isRequired
  }
  componentDidMount()
  {
    this.props.getStatistics();
  }
  render() {
      let apiVersion: any = this.props.statistics.backend.PROJECT_API_VERSION;
      if(apiVersion === 'dev')
      {
          apiVersion = (<i className="fab fa-dev" style={{
              fontSize: "1.5rem",
              color: "#ffff00"
          }}></i>);
      }
    return (
        <nav className="navbar hidden-sm-down fixed-bottom navbar-inverse bg-primary app-navbar">
            <span className="navbar-brand w-100" style={{
                lineHeight: "2rem"
            }}>
                Projekt STFN
                <span className="float-right text-light" style={{
                    fontSize: "0.9rem",
                    marginLeft: "5px"
                }}>
                    API: {apiVersion}
                </span>
                <span className="float-right text-light" style={{
                    fontSize: "0.9rem"
                }}>
                    Panel v{config.meta.clientVersion}
                </span>
            </span>
        </nav>
    )
  }
}

const mapStateToProps = (state) => ({
  statistics: state.statistics.statistics
})

export default connect(mapStateToProps, {getStatistics})(Footer)
