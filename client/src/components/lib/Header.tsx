import React, { Component } from 'react'
import { NavLink as Link } from "react-router-dom";
import config from '../../config';
export default class Header extends Component {
  render() {
    return (
        <nav className="navbar navbar-toggleable-sm fixed-top navbar-inverse bg-primary app-navbar">
            <button className="navbar-toggler navbar-toggler-right hidden-md-up" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <a className="navbar-brand" href="admin.php">
                STFN
            </a>
            <div className="collapse navbar-collapse mr-auto" id="navbarResponsive">
                <ul className="nav navbar-nav">
                    <li className="nav-item">
                        <Link to="/" className="nav-link" activeClassName="active" exact>
                            <i className="fas fa-home" /> Statystyki
                        </Link>
                    </li>
                    {/* <li className="nav-item">
                        <Link to="/" className="nav-link" activeClassName="active">
                            <i className="fas fa-suitcase" /> Projekty
                        </Link>
                    </li> */}
                    <li className="nav-item">
                        <Link to="/translations" className="nav-link" activeClassName="active">
                            <i className="fas fa-globe" /> Tłumaczenia
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link to="/translationadd" className="nav-link" activeClassName="active">
                            <i className="fas fa-plus" /> Dodaj Tłumaczenie
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link to="/notifications" className="nav-link" activeClassName="active">
                            <i className="fas fa-comments" /> Powiadomienia
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link to="/tester" className="nav-link" activeClassName="active">
                            <i className="fas fa-comments" /> Przetestuj API
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link to="/docs" className="nav-link" activeClassName="active">
                            <i className="fas fa-book" /> Dokumentacja
                        </Link>
                    </li>
                </ul>
            </div>
        </nav>
    )
  }
}
