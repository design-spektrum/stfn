import React, { Component } from 'react'
import Page from '../lib/Page';
import EditTranslationC from '../EditTranslation';

export default class EditTranslation extends Component<any, any> {
  render() {
    const { tid } = this.props.match.params;
    return (
      <Page>
        <EditTranslationC TID={tid}/>
      </Page>
    )
  }
}
