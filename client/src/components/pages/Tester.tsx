import React, { Component } from 'react'
import Page from '../lib/Page';
import TesterPage from '../TesterPage';

export default class Tester extends Component<any, any> {
  render() {
    return (
        <Page>
            <TesterPage/>
        </Page>
    )
  }
}
