import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers';
import config from './config';

const initialState = {};

const middleware = [
    thunk
];
const store = createStore(
    rootReducer,
    initialState,
    config.debug ? 
    compose(
        applyMiddleware(...middleware),
        (
            (window as any).__REDUX_DEVTOOLS_EXTENSION__ &&
            (window as any).__REDUX_DEVTOOLS_EXTENSION__({
                name: "STFN Redux Store",
                trace: true
            })
        ) || compose
    ) : applyMiddleware(...middleware)
);
export default store;