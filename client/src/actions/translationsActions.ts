import { 
  GET_TRANSLATIONS,
  ADD_TRANSLATION,
  DELETE_TRANSLATION,
  UPDATE_TRANSLATION,
  GET_TRANSLATION 
} from "./types";
import { ReduxAction } from '../lib/types';
import config from '../config';
import axios from "axios";

export const getTranslations = () => (dispatch: (action: ReduxAction) => void) => {
    config.API
      .get(config.API_url+"api/translations")
      .then(res => dispatch({
        type: GET_TRANSLATIONS,
        payload: res.data
      }))
      .catch(err =>
        console.error(err)
      );
  };
export const getTranslation = (tid: any) => (dispatch: (action: ReduxAction) => void) => {
    config.API
      .get(config.API_url+"api/translation", {params: {tid}})
      .then(res => dispatch({
        type: GET_TRANSLATION,
        payload: res.data.result
      }))
      .catch(err =>
        console.error(err)
      );
  };
const getTranslations_SA = async () => {
  return config.API
      .get(config.API_url+"api/translations")
      .catch(err =>
        console.error(err)
      );
}
export const addTranslations = (data: any) => (dispatch: (action: ReduxAction) => void) => {
  config.API
    .post(
      config.API_url+"api/translation",
      data
    )
    .then(res => {
      getTranslations_SA()
        .then(res2 => {
          dispatch({
            type: ADD_TRANSLATION,
            payload: (res2 as any).data
          });
          alert("Dodano frazę!");
        });
    })
    .catch(err =>
      console.error(err)
    );
  };
export const removeTranslation = (tid: any, callback: () => void) => (dispatch: (action: ReduxAction) => void) => {
    config.API
      .delete(config.API_url+"api/translation?tid="+encodeURIComponent(tid))
      .then(res => {
        dispatch({
          type: DELETE_TRANSLATION,
          payload: tid
        });
        callback();
      })
      .catch(err =>
        console.error(err)
      );
  };
export const editTranslations = (data: any) => (dispatch: (action: ReduxAction) => void) => {
  config.API
    .put(
      config.API_url+"api/translation",
      data
    )
    .then(res => {
      getTranslations_SA()
        .then(res2 => {
          dispatch({
            type: UPDATE_TRANSLATION,
            payload: (res2 as any).data
          });
          alert("Edytowano frazę!");
        });
    })
    .catch(err =>
      console.error(err)
    );
  };