import { 
  GET_NOTIFICATIONS,
  UPDATE_NOTIFICATION
} from "./types";
import { ReduxAction } from '../lib/types';
import config from '../config';
import axios from "axios";

export const getNotifications = () => (dispatch: (action: ReduxAction) => void) => {
    config.API
      .get(config.API_url+"api/notifications")
      .then(res => dispatch({
        type: GET_NOTIFICATIONS,
        payload: res.data.result
      }))
      .catch(err =>
        console.error(err)
      );
  };
// export const updateTranslations = (data: any) => (dispatch: (action: ReduxAction) => void) => {
//   config.API
//     .put(
//       config.API_url+"api/translation",
//       data
//     )
//     .then(res => {
//       getTranslations_SA()
//         .then(res2 => {
//           dispatch({
//             type: UPDATE_NOTIFICATION,
//             payload: (res2 as any).data
//           });
//           alert("Edytowano frazę!");
//         });
//     })
//     .catch(err =>
//       console.error(err)
//     );
//   };