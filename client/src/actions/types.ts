///////////////////     Public API   //////////////////
/**
 * Get Translation Action
 * ```sql
 * SELECT * FROM `translations`
 * ```
 */
export const GET_TRANSLATIONS: string = 'GET_TRANSLATIONS';
/**
 * Get Translation Action
 * ```sql
 * SELECT * FROM `translations` WHERE TID = :tid
 * ```
 */
export const GET_TRANSLATION: string = 'GET_TRANSLATION';
/**
 * Get Statistics Action
 * ```sql
 * SELECT * FROM `statistics`
 * ```
 */
export const GET_STATISTICS: string = 'GET_STATISTICS';
/////////////////// Admin Panel API  //////////////////
/**
 * Add New Translation
 * ```sql
 * INSERT INTO `translation` VALUES (:val)
 * ```
 */
export const ADD_TRANSLATION: string = 'ADD_TRANSLATION';
/**
 * Update Translation
 * ```sql
 * UPDATE `translation` SET :val
 * ```
 */
export const UPDATE_TRANSLATION: string = 'UPDATE_TRANSLATION';
/**
 * Deletes Translation
 * ```sql
 * DELETE FROM `translation` WHERE (:val)
 * ```
 */
export const DELETE_TRANSLATION: string = 'DELETE_TRANSLATION';
/**
 * Get Notifications
 * ```sql
 * SELECT * FROM `translation`
 * ```
 */
export const GET_NOTIFICATIONS: string = 'GET_NOTIFICATIONS';
/**
 * Update Notification
 * ```sql
 * UPDATE `translation` SET STATE=ZOBACZONE WHERE (:val)
 * ```
 */
export const UPDATE_NOTIFICATION: string = 'UPDATE_NOTIFICATION';
//export const 