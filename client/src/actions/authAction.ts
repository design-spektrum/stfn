import config from '../config';

export const authenticate = () => {
    config.API
      .get("admin/getJWT/designspektrum")
      .then(res => {
        localStorage["jwt"] = res.data;
      })
      .catch(err =>{
        console.group("Authentication");
        console.error(err);
        console.groupEnd();
      });
  };

// export const authenticateP = () => {
//     return (new Promise( (resolve, reject) => {
//       config.API
//         .get("admin/getJWT/designspektrum")
//         .then(res => {
//           localStorage["jwt"] = res.data;
//           resolve(res);
//         })
//         .catch(err =>{
//           console.group("Authentication");
//           console.error(err);
//           console.groupEnd();
//           reject(err);
//         });
//     }));
//   };
// export const getAuthToken = async () => {
//   if(localStorage["jwt"] !== undefined && localStorage["jwt"] !== null)
//   {
//     await authenticateP();
//   }
//   return localStorage["jwt"];
// }