import { GET_STATISTICS } from "./types";
import { ReduxAction } from '../lib/types';
import config from '../config';
import axios from "axios";

// export const getStatistics = () => {
//     return {
//         type: GET_STATISTICS
//     }
// }
export const getStatistics = () => (dispatch: (action: ReduxAction) => void) => {
    config.API
      .get("SettingsAPI/getsettings")
      .then(res => dispatch({
        type: GET_STATISTICS,
        payload: res.data
      }))
      .catch(err =>
        console.error(err)
      );
  };