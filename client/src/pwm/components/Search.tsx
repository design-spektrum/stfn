import React, { Component } from 'react';
import PropTypes from 'prop-types';

/**
 * Search PWM Component
 * Properties:
 * * classes - CSS Classes of Input for Searcher
 * * subject - Array to search from
 * * onFind(RESULTS) - Function that will receive results of search
 * * debug - Debug Flag - When enabled you will see in console some debug logs
 * * placeholder - Placeholder for input
 * * aria-label - Aria-Label for input
 * * aria-describedby - Aria-Describedby
 */
export default class Search extends Component<any, any> {
  /**
   * Properties Types
   */
  static propTypes = {
    classes: PropTypes.string,
    subject: PropTypes.array.isRequired,
    onFind: PropTypes.func.isRequired,
    debug: PropTypes.bool,
    placeholder: PropTypes.string,
    'aria-label': PropTypes.string,
    'aria-describedby': PropTypes.string
  }
  /**
   * Default Properties
   */
  static defaultProps = {
    debug: false
  };
  /**
   * Constructor of Component
   */
  constructor(props)
  {
    super(props);
    this.onChange = this.onChange.bind(this);
  }
  /**
   * Logging if debug in props is true
   * @param text Text to log
   * @param t Type [1 - group, 2 - groupend, 0/def - log]
   * @returns console on def/0
   */
  log(text: string = "", t: number = 0): any
  {
    if(this.props.debug)
    {
      switch(t)
      {
        case 1:
          console.group(text);
          break;
        case 2:
          console.groupEnd();
          break;
        case 0:
        default:
          return console.log;
      }
    }
    else
    {
      return function(){};
    }
  }
  /**
   * On Change Event Handler
   * @param e Event Object
   */
  onChange(e: any): void
  {
    this.log("Search Component Debug", 1);
    this.log()("Subject", this.props.subject);
    this.log()("Input Change occured", e);
    this.log()("Searching for:",e.target.value);
    let results: any[];
    if(e.target.value !== null && e.target.value !== undefined && e.target.value !== "")
    {
      const search = new RegExp(e.target.value);
      this.log()("RegExp Created", search);
      results = this.props.subject.filter((val: any) => {
        if(search.test(val))
        {
          return val;
        }
      });
    }
    else
    {
      results = this.props.subject;
    }
    this.log()("Searching complete", results);
    this.log()("onFind - called")
    this.props.onFind(results);
    this.log("", 2);
  }
  /**
   * Render Method
   */
  render() {
    return (
      <input className={this.props.classes} placeholder={this.props.placeholder} aria-label={this.props['aria-label']} aria-describedby={this.props['aria-describedby']} onChange={this.onChange}/>
    )
  }
}
