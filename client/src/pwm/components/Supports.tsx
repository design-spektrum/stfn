import React, { Component } from 'react'
import PropTypes from 'prop-types'

/**
 * Component for checking support
 * Using is.js checking made inside component.
 * 
 * Properties:
 * * type - is it should be `if` or `else` [Default if]
 * * tags - What tags should/shoudn't be supported
 * 
 * Possible tags:
 * * android
 * * androidPhone
 * * androidTablet
 * * blackberry
 * * chrome
 * * ipad
 * * iphone
 * * ipod
 * * mobile
 * * tablet
 * * desktop
 * * edge
 * * firefox
 * * ie
 * * ios
 * * linux
 * * mac
 * * online
 * * offline
 * * opera
 * * operaMini
 * * phantom
 * * safari
 * * windows
 * * windowsPhone
 * * windowsTable
 */
export default class Supports extends Component<any, any> {
  /**
   * Properties Types
   */
  static propTypes = {
    type: PropTypes.string.isRequired,
    tags: PropTypes.array.isRequired
  }
  /**
   * Default Properties
   */
  static defaultProps = {
    type: "if",
  };
  constructor(props)
  {
    super(props);
    this.check = this.check.bind(this);
  }
  static is()
  {
    let freeSelf = (window as any);
    let navigator = freeSelf && freeSelf.navigator;
    let platform = (navigator && navigator.platform || '').toLowerCase();
    let userAgent = (navigator && navigator.userAgent || '').toLowerCase();
    let vendor = (navigator && navigator.vendor || '').toLowerCase();
    
    // build a 'comparator' object for various comparison checks
    var comparator = {
      '<': function(a, b) { return a < b; },
      '<=': function(a, b) { return a <= b; },
      '>': function(a, b) { return a > b; },
      '>=': function(a, b) { return a >= b; }
    };
    // helper function which compares a version to a range
    function compareVersion(version, range) {
      var string = (range + '');
      var n = +(string.match(/\d+/) || NaN);
      var op = string.match(/^[<>]=?|/)[0];
      return comparator[op] ? comparator[op](version, n) : (version == n || n !== n);
    }
    // is current device android?
    let android = function() {
      return /android/.test(userAgent);
    };

    // is current device android phone?
    let androidPhone = function() {
        return /android/.test(userAgent) && /mobile/.test(userAgent);
    };

    // is current device android tablet?
    let androidTablet = function() {
        return /android/.test(userAgent) && !/mobile/.test(userAgent);
    };

    // is current device blackberry?
    let blackberry = function() {
        return /blackberry/.test(userAgent) || /bb10/.test(userAgent);
    };

    // is current browser chrome?
    // parameter is optional
    let chrome = function(range = undefined) {
        var match = /google inc/.test(vendor) ? userAgent.match(/(?:chrome|crios)\/(\d+)/) : null;
        return match !== null && !opera() && compareVersion(match[1], range);
    };
    // is current device ipad?
    // parameter is optional
    let ipad = function(range = undefined) {
        var match = userAgent.match(/ipad.+?os (\d+)/);
        return match !== null && compareVersion(match[1], range);
    };

    // is current device iphone?
    // parameter is optional
    let iphone = function(range = undefined) {
        // avoid false positive for Facebook in-app browser on ipad;
        // original iphone doesn't have the OS portion of the UA
        var match = ipad() ? null : userAgent.match(/iphone(?:.+?os (\d+))?/);
        return match !== null && compareVersion(match[1] || 1, range);
    };

    // is current device ipod?
    // parameter is optional
    let ipod = function(range = undefined) {
        var match = userAgent.match(/ipod.+?os (\d+)/);
        return match !== null && compareVersion(match[1], range);
    };
    // is current device mobile?
    let mobile = function() {
        return iphone() || ipod() || androidPhone() || blackberry() || windowsPhone();
    };

    // is current device tablet?
    let tablet = function() {
        return ipad() || androidTablet() || windowsTablet();
    };
    // is current device desktop?
    let desktop = function() {
        return !mobile() && !tablet();
    };

    // is current browser edge?
    // parameter is optional
    let edge = function(range=undefined) {
        var match = userAgent.match(/edge\/(\d+)/);
        return match !== null && compareVersion(match[1], range);
    };

    // is current browser firefox?
    // parameter is optional
    let firefox = function(range=undefined) {
        var match = userAgent.match(/(?:firefox|fxios)\/(\d+)/);
        return match !== null && compareVersion(match[1], range);
    };

    // is current browser internet explorer?
    // parameter is optional
    let ie = function(range=undefined) {
        var match = userAgent.match(/(?:msie |trident.+?; rv:)(\d+)/);
        return match !== null && compareVersion(match[1], range);
    };

    // is current device ios?
    let ios = function() {
        return iphone() || ipad() || ipod();
    };


    // is current operating system linux?
    let linux = function() {
        return /linux/.test(platform) && !android();
    };

    // is current operating system mac?
    let mac = function() {
        return /mac/.test(platform);
    };

    // is current state online?
    let online = function() {
      return !navigator || navigator.onLine === true;
    };
    // is current state offline?
    let offline = !(online);

    // is current browser opera?
    // parameter is optional
    let opera = function(range = undefined) {
        var match = userAgent.match(/(?:^opera.+?version|opr)\/(\d+)/);
        return match !== null && compareVersion(match[1], range);
    };

    // is current browser opera mini?
    // parameter is optional
    let operaMini = function(range = undefined) {
        var match = userAgent.match(/opera mini\/(\d+)/);
        return match !== null && compareVersion(match[1], range);
    };

    // is current browser phantomjs?
    // parameter is optional
    let phantom = function(range = undefined) {
        var match = userAgent.match(/phantomjs\/(\d+)/);
        return match !== null && compareVersion(match[1], range);
    };

    // is current browser safari?
    // parameter is optional
    let safari = function(range = undefined) {
        var match = userAgent.match(/version\/(\d+).+?safari/);
        return match !== null && compareVersion(match[1], range);
    };

    // is current operating system windows?
    let windows = function() {
        return /win/.test(platform);
    };

    // is current device windows phone?
    let windowsPhone = function() {
        return windows() && /phone/.test(userAgent);
    };

    // is current device windows tablet?
    let windowsTablet = function() {
        return windows() && !windowsPhone() && /touch/.test(userAgent);
    };
    return {
      android: android(),
      androidPhone: androidPhone(),
      androidTablet: androidTablet(),
      blackberry: blackberry(),
      chrome: chrome(),
      ipad: ipad(),
      iphone: iphone(),
      ipod: ipod(),
      mobile: mobile(),
      tablet: tablet(),
      desktop: desktop(),
      edge: edge(),
      firefox: firefox(),
      ie: ie(),
      ios: ios(),
      linux: linux(),
      mac: mac(),
      online: online(),
      offline,
      opera: opera(),
      operaMini: operaMini(),
      phantom: phantom(),
      safari: safari(),
      windows: windows(),
      windowsPhone: windowsPhone(),
      windowsTablet: windowsTablet()
    };
  }
  check()
  {
    const tags: string[] = this.props.tags;
    const IS = Supports.is();
    let result = false;
    tags.forEach(element => {
      result = result || IS[element];
    });
    switch(this.props.type)
    {
      case 'else':
        return !result;
      case 'if':
      default:
        return result;
    }
  }
  render() {
    if(this.check())
    {
      return this.props.children;
    }
    else
    {
      return null;
    }
  }
}
