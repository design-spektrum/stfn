import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Supports from './Supports';

/**
 * Date Time PolyFill Component
 * 
 * Properties:
 * * value - Value of DateTime
 * * onChange - Function `onChange(e: Event, value: any)`
 * * placeholder - Placeholder of DateTime
 * * className - className of DateTime
 * * id - Id of DateTime
 */
export default class DateTime extends Component<any, any> {
  static propTypes = {
    value: PropTypes.any,
    onChange: PropTypes.func.isRequired,
    placeholder: PropTypes.string,
    className: PropTypes.string,
    id: PropTypes.string
  }
  constructor(props)
  {
    super(props);
    this.onChange = this.onChange.bind(this);
    this.onChangeC = this.onChangeC.bind(this);
    this.getValue = this.getValue.bind(this);
  }
  onChange(e)
  {
    this.props.onChange(e, e.target.value);
  }
  onChangeC(e)
  {
    this.props.onChange(e, e.target.value);
  }
  getValue(type: string = "datetime")
  {
    let val = this.props.value;
    if(type === "datetime")
    {
      let val2 = new Date(val);
      val = val2.toISOString();
    }
    else if(type === "time")
    {
      let val2 = new Date(val);
      val = val2.toTimeString();
    }
    else if(type === "date")
    {
      let val2 = new Date(val);
      val = val2.toDateString();
    }
    return val;
  }
  render() {
    console.log("Render of DateTime");
    const browserSupport = ['edge', 'chrome', 'opera', 'blackberry'];
    return (
      <div>
        <Supports type="if" tags={browserSupport}>
          <input
            type="datetime-local"
            onChange={this.props.onChange}
            className={this.props.className}
            placeholder={this.props.placeholder}
            id={this.props.id}
            value={this.getValue("datetime")}
          />
        </Supports>
        <Supports type="else" tags={browserSupport}>
          <input
            type="date"
            onChange={this.props.onChangeC}
            className={this.props.className}
            placeholder={this.props.placeholder}
            id={this.props.id}
            value={this.getValue("date")}
          />
          <input
            type="time"
            onChange={this.props.onChangeC}
            className={this.props.className}
            placeholder={this.props.placeholder}
            id={this.props.id}
            value={this.getValue("time")}
          />
        </Supports>
      </div>
    )
  }
}
