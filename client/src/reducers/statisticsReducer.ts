import { GET_STATISTICS } from '../actions/types';
import { ReduxAction } from '../lib/types';
import config from '../config';
const initialState = {
    statistics: {
        backend: {},
        frontend: {}
    }
};

export default function (state : object = initialState, action: ReduxAction)
{
    switch(action.type)
    {
        case GET_STATISTICS:
            return {
                ...state,
                statistics: {
                    backend: {
                        ...action.payload.settings
                    },
                    frontend: {
                        debugMode: config.debug,
                        apiURL: config.API_url,
                        ...config.meta
                    }
                }
            };
        default:
            return state;
    }
}