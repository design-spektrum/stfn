import { GET_NOTIFICATIONS, UPDATE_NOTIFICATION } from '../actions/types';
import { ReduxAction } from '../lib/types';
import config from '../config';
const initialState = {
    statistics: {
        backend: {},
        frontend: {}
    }
};

export default function (state : object = initialState, action: ReduxAction)
{
    switch(action.type)
    {
        case GET_NOTIFICATIONS:
            return {
                ...state,
                notifications: action.payload
            };
        default:
            return state;
    }
}