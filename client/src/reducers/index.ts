import { combineReducers } from 'redux';
import translationReducer from './translationReducer';
import statisticsReducer from './statisticsReducer';
import notificationsReducer from './notificationsReducer';

export default combineReducers({
    translation: translationReducer,
    statistics: statisticsReducer,
    notification: notificationsReducer
});