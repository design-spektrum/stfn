import { 
    GET_TRANSLATIONS,
    ADD_TRANSLATION,
    DELETE_TRANSLATION,
    UPDATE_TRANSLATION,
    GET_TRANSLATION
} from "../actions/types";
import { ReduxAction } from '../lib/types';

const initialState = {};

export default function (state: object = initialState, action: ReduxAction)
{
    switch(action.type)
    {
        case GET_TRANSLATIONS:
            const base_gt: object[] = action.payload.result !== undefined ? action.payload.result : [];
            let tmp_gt: object[] = base_gt.filter((val) => {
                delete val['status'];
                return val;
            })
            return {
                ...state,
                translations: tmp_gt
            };
        case ADD_TRANSLATION:
            const base_at: object[] = action.payload.result !== undefined ? action.payload.result : [];
            let tmp_at: object[] = base_at.filter((val) => {
                delete val['status'];
                return val;
            })
            return {
                ...state,
                translations: tmp_at
            };
        case DELETE_TRANSLATION:
            // console.log(DELETE_TRANSLATION, action.payload, (state as any).translations.filter( val => val.tid != action.payload))
            return {
                ...state,
                translations: (state as any).translations.filter( val => val.tid != action.payload)
            };
        case UPDATE_TRANSLATION:
            const base_ut: object[] = action.payload.result !== undefined ? action.payload.result : [];
            let tmp_ut: object[] = base_ut.filter((val) => {
                delete val['status'];
                return val;
            })
            return {
                ...state,
                translations: tmp_ut
            };
        case GET_TRANSLATION:
            let tmp_gt1: object = action.payload;
            delete tmp_gt1["status"];
            return {
                ...state,
                translation: tmp_gt1
            };
        default:
            return state;
    }
}