import config from '../config';

export function apiVersionURL(version: string|number):string {
    return `${config.API_url}/`;
    // Przyszłość
    if(version === "dev" || version === "latest")
    {
        return `${config.API_url}/`;
    }
    else
    {
        return `${config.API_url}api/v${version}/`;
    }
}
export function fullAPIURL(version: string|number, url: string):string {
    return `${apiVersionURL(version)}${url}`;
}
export const sleep = (ms) => new Promise(resolve => setTimeout(resolve, ms));