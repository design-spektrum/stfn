import config from '../config';
function langPath(...args:any[])
{
    return args.join(config.translation_separator);
}
function getLanguage() : object
{
    let lang: object = {};
    lang[langPath('stats','DEVELOPMENT')] = "Tryb Deweloperski";
    lang[langPath('stats','PROJECT_MODE')] = "Tryb Projektu";
    lang[langPath('stats','VERBOSE')] = "Poziom Raportowania";
    lang[langPath('stats','DB_HOST')] = "Nazwa Hosta Bazy Danych";
    lang[langPath('stats','DB_NAME')] = "Nazwa Bazy Danych";
    lang[langPath('stats','APPROOT')] = "Ścieżka do aplikacji na serwerze";
    lang[langPath('stats','URLROOT')] = "Ścieżka bazowa do aplikacji";
    lang[langPath('stats','PROJECT_VERSION')] = "Wersja projektu";
    lang[langPath('stats','PROJECT_FULL_VERSION')] = "Pełna Wersja Projektu";
    lang[langPath('stats','PROJECT_API_VERSION')] = "Wersja API";
    lang[langPath('stats','debugMode')] = "Tryb Deweloperski";
    lang[langPath('stats','apiURL')] = "Adres bazowy API";
    lang[langPath('stats','react')] = "Wersja React";
    lang[langPath('stats','redux')] = "Wersja Redux";
    lang[langPath('stats','react-redux')] = "Wersja React-Redux";
    lang[langPath('stats','clientVersion')] = "Wersja Panelu Administracyjnego";
    lang[langPath('stats','DEVELOPMENT','true')] = "Włączony";
    lang[langPath('stats','DEVELOPMENT','false')] = "Wyłączony";
    lang[langPath('stats','PROJECT_MODE','0')] = "Produkcyjny";
    lang[langPath('stats','PROJECT_MODE','1')] = "Testowy";
    lang[langPath('stats','PROJECT_MODE','2')] = "Deweloperski";
    lang[langPath('stats','VERBOSE','0')] = 'Brak';
    lang[langPath('stats','VERBOSE','1')] = 'Lekkie Logi';
    lang[langPath('stats','VERBOSE','2')] = 'Normalne Logi';
    lang[langPath('stats','VERBOSE','3')] = 'Pełne Logi';
    lang[langPath('stats','VERBOSE','255')] = 'Deweloperskie Logi';
    lang[langPath('stats','PROJECT_API_VERSION','dev')] = "Wersja Deweloperska (nie stabilna)";
    lang[langPath('stats','debugMode','true')] = "Włączony";
    lang[langPath('stats','debugMode','false')] = "Wyłączony";
    return lang;
}
export const lang: object = getLanguage();
export const sections: string[] = [
    'stats'
];
export const translate = (phrase:string): string => {
    // console.log(phrase);
    if(Object.keys(lang).includes(phrase))
    {
        return lang[phrase];
    }
    const phrase_t = phrase.split(config.translation_separator);
    if(sections.includes(phrase_t[0]))
    {
        return lang[phrase] || (phrase_t.length === 3) ? phrase_t[2] : phrase_t[1]
    }
    switch(phrase)
    {
        default:
            return phrase;
    }
};
