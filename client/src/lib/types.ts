import { AxiosInstance } from "axios";
export interface ReduxAction
{
    type: string;
    payload: any;
}
export interface STFNMetaConfig
{
    react: string,
    redux: string,
    'react-redux': string,
    clientVersion: string
}
export interface STFNConfig
{
    debug: boolean,
    API_url: string,
    translation_separator: string,
    meta: STFNMetaConfig,
    API: AxiosInstance
}