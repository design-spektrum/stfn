import React, { Component } from 'react';
function flag(name: string) : string {
  try {
    return require(`../lang_flags/${name}.png`);
  } catch (error) {
    return require(`../lang_flags/_default_.png`);
  }
}
/**
 * Get Flag for Language
 * @param lang Language
 * @returns Element with flag
 */
export default function getFlag(lang: string) : any {
  return <img src={flag(lang)} width="32" height="32"/>;
}