import React, { Component } from 'react';
import Page from '../components/lib/Page';
import Section from './components/Section';

export default class Index extends Component {
  render() {
    return (
      <Page>
        <Section name="intro" title="Witamy w dokumentacji STFN!">
          <p>
            Ta sekcja zostanie uzupełniona później.
          </p>
        </Section>
      </Page>
    )
  }
}
