import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class Section extends Component<any, any> {
  static propTypes = {
    title: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  }
  render() {
    return (
      <div id={this.props.name}>
        <h2>{this.props.title}</h2>
        {this.props.children}
      </div>
    )
  }
}
