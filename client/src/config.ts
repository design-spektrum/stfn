import axios, { AxiosInstance } from "axios";
import { STFNConfig } from './lib/types';
// import {getAuthToken} from './actions/authAction';
const config : STFNConfig = {
    debug: true,
    // API_url: 'http://192.168.1.199:90/stfn/',
    // API_url: 'https://192.168.1.199/stfn/',
    API_url: 'https://serwer1523088.home.pl/stfn/public/',
    translation_separator: '%E%',
    meta: {
        react: '16.7.0',
        redux: '4.0.1',
        'react-redux': '6.0.0',
        clientVersion: '1.0.0beta1'
    },
    API: null
};
config.API = axios.create({
    baseURL: config.API_url,
    params: {
        "S3CR3TN4M3": "0617b15d-b130-4713-8b35-b64b7b1a935c"
    },
    method: "get"
});
export default config;