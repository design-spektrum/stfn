# STFN

System Tłumaczeń Fraz Nieznanych

##################################################################################
```markdown
# @Patryk Adamczyk - Ten projekt jest cześcią projektu PAiP Web Translation API
```
##################################################################################

## Technologie
* PHP > 5.5
* [PAiP Web Framework PHP API Edition v0](https://gitlab.com/paip-web/pwf-php-api-edition/commit/f364ac1ffd6c705840aee0f0472e8938c8cda053) + [PAiP Web Framework PHP Edition v0 Controller Class](https://gitlab.com/paip-web/pwf-php/blob/9627fb55f37507659b73a0b2a08235c9d35a83e6/app/libraries/Controller.php)
* React 16.7


Notatka:

W Projekcie PAiP Web Translation API i w tym poźniej można zaktualizować <abbr title="PAiP Web Framework">PWF</abbr> do **0.0.4dev0**.

<div>Icons made by <a href="http://www.freepik.com/" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" 		    title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 		    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>